/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum;

/**
 * Author: adrian.libotean
 * Date: Dec 2, 2009
 * Time: 12:21:24 PM
 * Summary: Redirects console output to system.out and system.err
 */
public class SystemConsole implements Console {

    public SystemConsole() {
        super();
    }

    public void startUp() {}
    public void shutdown() {}

    public Console print(final CharSequence sequence) {
        System.out.print(sequence);
        return this;
    }

    public Console println(final CharSequence sequence) {
        System.out.println(sequence);
        return this;
    }

    public void flush() {
        System.out.flush();
        System.err.flush();
    }

    public void error(final String message) {
        System.err.print(message);
    }

    public void error(final String message, final Throwable ex) {
        System.err.print(message);
        ex.printStackTrace(System.err);
    }

    public void warning(String message) {
        System.err.print(message);
    }

    public void info(String message) {
        System.err.print(message);
    }
}