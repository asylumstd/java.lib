/*
 * Copyright (c) 2011 Asylum Studios Romania.
 */

package org.asylum;


/**
 * Author: adrian.libotean
 * Date: 08/04/2011
 * Time: 12:31:35 PM
 * Summary:
 */
public interface Task {
    void execute();

    String getTitle();
    
    boolean cancel(boolean force);

    void onCanceled();
    void onFinished();
}