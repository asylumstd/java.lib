package org.asylum.i18n;

/**
 * Author: adrian.libotean
 * Date: Nov 10, 2010
 * Time: 7:57:26 AM
 * Summary:
 */
public class Localizable {
    private Class klass;
    private String id;

    private Localizable(Class klass, String id) {
        this.klass = klass;
        this.id = id;
    }

    public Class getSourceClass() {
        return klass;
    }

    public String getID() {
        return id;
    }

    public int hashCode() {
        if (klass == null) {
            return id.hashCode();
        } else {
            return klass.hashCode() + id.hashCode();
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj instanceof Localizable) {
            final Localizable candidate = (Localizable) obj;

            if (klass == null) {
                return id.equalsIgnoreCase(candidate.id);
            } else {
                return (klass.equals(candidate.klass) && id.equalsIgnoreCase(candidate.id));
            }
        }

        return false;
    }

    public static Localizable factory(final Class klass, final String id) {
        return new Localizable(klass, id);
    }

    public static Localizable factory(final Object instance, final String id) {
        return factory(instance.getClass(), id);
    }

    public static Localizable factory(final String id) {
        return new Localizable(null, id);
    }
}