/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.i18n;

import java.text.Format;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.MissingResourceException;
import java.util.logging.Logger;
import java.util.logging.Level;

import org.asylum.cache.WeakCache;
import org.asylum.cache.ConcurrentSoftCache;

/**
 * Author: adrian.libotean
 * Date: Dec 9, 2009
 * Time: 2:14:20 PM
 */
public class BasicPolyglot implements Polyglot {
    private static final Logger LOG = Logger.getLogger(BasicPolyglot.class.getName());

    private static final String DEFAULT_CONTEXT = "default";
    private static final Locale DEFAULT_LOCALE = Locale.ENGLISH;
    
    private final WeakCache<Localizable, String> CACHE;

    public BasicPolyglot() {
        CACHE = new ConcurrentSoftCache<Localizable, String>() {
            public String generate(Localizable key) {
                return i18n(key);
            }
        };
    }

    public String i18n(CharSequence key) {
        return i18n(key, DEFAULT_CONTEXT);
    }

    public String i18n(CharSequence key, CharSequence context) {
        return null;
    }

    public String i18n(CharSequence key, Class klass) {
        return null;
    }

    public String i18n(Localizable item) {
        return i18n(item, DEFAULT_CONTEXT);
    }

    public String i18n(Localizable item, CharSequence context) {
        String result = CACHE.load(item);
        if (result != null) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Cache hit: " + item.getID());
            }

            return result;
        } else if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Cache miss :" + item.getID());
        }

        final String fileName = getFileName(item);

        if (LOG.isLoggable(Level.INFO)) {
            LOG.log(Level.INFO, "Translating '" + item.getID() + "'. Using bundle: " + fileName);
        }

        final ResourceBundle bundle;
        if (item.getSourceClass() != null) {
            bundle = ResourceBundle.getBundle(fileName, DEFAULT_LOCALE, item.getSourceClass().getClassLoader());
        } else {
            bundle = ResourceBundle.getBundle(fileName, DEFAULT_LOCALE);
        }

        try {
            result = bundle.getString(item.getID());
            return CACHE.store(item, result);
        } catch (MissingResourceException ex) {
            LOG.log(Level.WARNING, "Missing resource: " + ex.getMessage());
        }

        return null;
    }

    public String getCurrency() {
        return null;
    }

    public Format getPositiveCurrencyFormat() {
        return null;
    }

    public Format getNegativeCurrencyFormat() {
        return null;
    }

    public Format getNumericFormat() {
        return null;
    }

    public String getDecimalSymbol() {
        return null;
    }

    public String getGroupingSymbol() {
        return null;
    }

    public Format getTimeFormat() {
        return null;
    }

    public String getTimeSeparator() {
        return null;
    }

    public Format getShortDateFormat() {
        return null;
    }

    public Format getLongDateFormat() {
        return null;
    }

    public String getDateSeparator() {
        return null;
    }

    public MessageFormat getMessageFormatter() {
        return _getMessageFormatter("");
    }

    public MessageFormat getMessageFormatter(final String pattern) {
        return _getMessageFormatter(pattern);
    }

    private MessageFormat _getMessageFormatter(final String pattern) {
        MessageFormat result = new MessageFormat(pattern);
        result.setLocale(DEFAULT_LOCALE);
        return result;
    }

    private String getFileName(final Localizable item) {
        if (item.getSourceClass() == null) {
            return "Application";
        } else {
            final String packageName = item.getSourceClass().getPackage().getName();
            return packageName + ".resources." + item.getSourceClass().getSimpleName();
        }
    }
}