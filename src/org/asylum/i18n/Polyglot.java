/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.i18n;

import java.text.Format;
import java.text.MessageFormat;

/**
 * Author: adrian.libotean
 * Date: Dec 9, 2009
 * Time: 2:05:06 PM
 */
public interface Polyglot {
    String i18n(final CharSequence key, final Class klass);
    String i18n(final CharSequence key, final CharSequence context);
    String i18n(final CharSequence key);

    String i18n(final Localizable item);
    String i18n(final Localizable item, final CharSequence context);

    String getCurrency();
    Format getPositiveCurrencyFormat();
    Format getNegativeCurrencyFormat();
    Format getNumericFormat();

    String getDecimalSymbol();
    String getGroupingSymbol();

    Format getTimeFormat();
    String getTimeSeparator();

    Format getShortDateFormat();
    Format getLongDateFormat();
    String getDateSeparator();

    MessageFormat getMessageFormatter();
    MessageFormat getMessageFormatter(String pattern);
}