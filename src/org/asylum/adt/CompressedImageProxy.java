/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.adt;

import org.asylum.cache.Cache;
import org.asylum.cache.SoftCache;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Author: Adrian Libotean
 * Date: Aug 1, 2009
 * Time: 12:35:46 PM
 * Summary: A proxy that maintains a soft reference to an uncompressed image and will re-generate it when necessary
 *          from a compressed version
 */
public class CompressedImageProxy implements Icon {
    // bug: move caching inside glyph service
    private static final Cache<CompressedImage, ImageIcon> CACHE = new SoftCache<CompressedImage, ImageIcon>() {
        public ImageIcon generate(CompressedImage image) {
            return decompress(image);
        }
    };
    //--
    private final CompressedImage source;

    public CompressedImageProxy(final CompressedImage source) {
        super();

        this.source = source;
    }

    private Icon getImage() {
        ImageIcon result;
        synchronized (CACHE) {
            result = CACHE.load(source);
        }

        if (result == null) {
            result = decompress(source);

            synchronized (CACHE) {
                CACHE.store(source, result);
            }
        }

        return result;
    }

    public void paintIcon(Component c, Graphics g, int x, int y) {
        getImage().paintIcon(c, g, x, y);
    }

    public int getIconWidth() {
        return getImage().getIconWidth();
    }

    public int getIconHeight() {
        return getImage().getIconHeight();
    }

    private static ImageIcon decompress(final CompressedImage source) {
        try {
            Object stream = new ByteArrayInputStream(source.getJpegData());
            return new ImageIcon(ImageIO.read(ImageIO.createImageInputStream(stream)));
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}