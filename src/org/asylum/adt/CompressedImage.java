/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.adt;

import org.asylum.ArraysUtil;

import javax.imageio.ImageIO;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;

/**
 * Author: adrian.libotean
 * Date: May 11, 2007
 * Time: 2:10:20 PM
 * Summary: A lightweight and serializable image
 */
public class CompressedImage implements Serializable {
    static final long serialVersionUID = 2491470912029288646L;

    private long id;
    private String name;
    private byte[] imageData;

    @SuppressWarnings({"TypeMayBeWeakened"})
    public CompressedImage(RenderedImage image) {
        super();

        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            ImageIO.write(image, "jpeg", ImageIO.createImageOutputStream(stream));
            imageData = stream.toByteArray();
        } catch (IOException ex) {
            imageData = null;
            ex.printStackTrace();
        }
    }

    public long getID() {
        return id;
    }

    public void setID(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public byte[] getJpegData() {
        return ArraysUtil.copy(imageData);
    }
}