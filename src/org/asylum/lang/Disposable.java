/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.lang;

/**
 * Author: adrian.libotean
 * Date: Dec 2, 2009
 * Time: 10:56:26 AM
 */
public interface Disposable {
    void dispose();

    interface Parent extends Disposable {
        void beforeTreeDispose();
    }
}