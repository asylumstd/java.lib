/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.lang;

import org.asylum.Console;
import org.asylum.configuration.Configurable;
import static org.asylum.services.ServiceRegistry.getService;

import java.util.*;

/**
 * Author: adrian.libotean
 * Date: Dec 2, 2009
 * Time: 10:56:32 AM
 * Summary: Manages a disposable tree and tracks reference leaks for registered disposables 
 */
public class Disposer {
    private static final Configurable<Boolean> debugMode = new Configurable<Boolean>(Boolean.TRUE);
    private static final Map<Disposable, List<Disposable>> tree = new HashMap<Disposable, List<Disposable>>();
    private static final Map<Disposable, Throwable> stacks = new HashMap<Disposable, Throwable>();
    private static final Set<Disposable> disposeQueue = new HashSet<Disposable>();

    static {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                checkReferenceLeaks();
            }
        });
    }

    public static void register(final Disposable parent) {
        register(parent, null);
    }

    @SuppressWarnings({"ThrowableInstanceNeverThrown", "ThrowableResultOfMethodCallIgnored"})
    public static void register(final Disposable parent, final Disposable child) {
        //-- collect stack information if in debug mode
        if (debugMode.get().booleanValue()) {
            synchronized (stacks) {
                stacks.put(parent, new Throwable());
                if (child != null) {
                    stacks.put(child, new Throwable());
                }
            }
        }

        synchronized (tree) {
            List<Disposable> children;

            if (!tree.containsKey(parent)) {
                children = new ArrayList<Disposable>();
                tree.put(parent, children);
            } else {
                children = tree.get(parent);
            }

            if (child != null) {
                children.add(child);
            }
        }
    }

    public static void dispose(final Disposable item) {
        synchronized (tree) {
            List<Disposable> children = tree.get(item);

            if (children != null) {
                tree.remove(item);

                if (item instanceof Disposable.Parent) {
                    ((Disposable.Parent) item).beforeTreeDispose();
                }

                //-- dispose children
                for (final Disposable child : children) {
                    dispose(child);
                }

                //-- dispose item
                doDispose(item);
            } else {
                //-- remove all other occurrences of item
                for (Map.Entry<Disposable, List<Disposable>> entry : tree.entrySet()) {
                    if (entry.getValue().contains(item)) {
                        entry.getValue().remove(item);
                    }
                }

                //-- dispose item
                doDispose(item);
            }
        }

        synchronized (disposeQueue) {
            disposeQueue.remove(item);
        }
    }

    static int getSize() {
        synchronized (tree) {
            return tree.size();
        }
    }

    static boolean contains(final Disposable disposable) {
        synchronized (tree) {
            if (tree.containsKey(disposable)) {
                return true;
            }

            for (Map.Entry<Disposable, List<Disposable>> entry : tree.entrySet()) {
                if (entry.getValue().contains(disposable)) {
                    return true;
                }
            }
        }

        return false;
    }

    static int getChildrenCount(final Disposable parent) {
        synchronized (tree) {
            final List<Disposable> children = tree.get(parent);
            return (children != null) ? children.size() : 0;
        }
    }

    @SuppressWarnings({"ThrowableResultOfMethodCallIgnored"})
    private static void checkReferenceLeaks() {
        if (debugMode.get().booleanValue()) {
            final Console console = getService(Console.class);

            console.println(String.format("%s leak(s) detected", Integer.valueOf(getSize())));

            synchronized (tree) {
                synchronized (stacks) {
                    for (Map.Entry<Disposable, List<Disposable>> entry : tree.entrySet()) {
                        console.println("Leak detected =========================");

                        final Disposable parent = entry.getKey();
                        console.error("", stacks.get(parent));

                        console.println("").println("Children: ").println("");

                        for (final Disposable item : entry.getValue()) {
                            console.error("", stacks.get(item));
                        }

                        console.println("=======================================").println("");
                    }
                }
            }
        }
    }

    private static void doDispose(final Disposable item) {
        //-- skip if already in dispose queue
        synchronized (disposeQueue) {
            if (disposeQueue.contains(item)) {
                return;
            }
        }

        try {
            synchronized (disposeQueue) {
                disposeQueue.add(item);
            }

            item.dispose();
        } catch (RuntimeException ex) {
            final Console console = getService(Console.class);
            console.error("Exception thrown while disposing", ex);
        }
    }
}