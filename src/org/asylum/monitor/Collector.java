/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.monitor;

/**
 * Author: adrian.libotean
 * Date: Nov 27, 2009
 * Time: 11:59:01 AM
 * Summary: Simple interface implemented by all resources/performance collectors
 */
public interface Collector<T> {
    Policy getPolicy();
    T collect();

    public static enum Policy {
        Snapshot,
        Auto,
    }
}