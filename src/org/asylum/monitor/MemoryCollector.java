/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.monitor;

/**
 * Author: adrian.libotean
 * Date: Nov 27, 2009
 * Time: 12:05:09 PM
 */
public class MemoryCollector implements Collector<MemoryCollector.MemoryBundle> {

    public Policy getPolicy() {
        return Policy.Snapshot;
    }

    public MemoryBundle collect() {
        Runtime runtime = Runtime.getRuntime();
        long total = runtime.maxMemory();
        long free = runtime.freeMemory();

        return new MemoryBundle(total, free);
    }

    public static class MemoryBundle {
        private final long total;
        private final long occupied;
        private final long free;
        private final long occupiedPercent;
        private final long freePercent;

        public MemoryBundle(long total, long free) {
            this.total = total;
            this.occupied = total - free;
            this.free = free;

            this.occupiedPercent = 100 - (free * 100 / total);
            this.freePercent = free * 100 / total;
        }

        public long getTotal() {
            return total;
        }

        public long getOccupied() {
            return occupied;
        }

        public long getFree() {
            return free;
        }

        public long getOccupiedPercent() {
            return occupiedPercent;
        }

        public long getFreePercent() {
            return freePercent;
        }
    }
}