/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.monitor;

import org.asylum.configuration.Configurable;
import org.asylum.lang.Disposable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Author: Adrian Libotean
 * Date: Jul 30, 2009
 * Time: 1:38:31 PM
 * Summary: Simple resource monitor/watchdog
 */
public class WatchdogImpl implements Watchdog, Disposable {
    private static final Configurable<Integer> DEFAULT_SNAPSHOT_INTERVAL = new Configurable<Integer>(Integer.valueOf(3000));
    //--
    private final Collection<Collector<?>> collectors;
    private final Collection<GenericObserver> observers;
    private MonitorThread monitor;
    private int snapshotInterval;
    private Collection lastSnapshot;

    public WatchdogImpl(int snapshotInterval) {
        super();

        this.snapshotInterval = (snapshotInterval <= 0) ? DEFAULT_SNAPSHOT_INTERVAL.get().intValue() : snapshotInterval;
        this.collectors = new ArrayList<Collector<?>>();
        this.observers = new ArrayList<GenericObserver>();
        this.monitor = null;
        this.lastSnapshot = null;
    }

    public WatchdogImpl() {
        this(-1);
    }

    public void dispose() {
        if (monitor != null) {
            monitor.terminate();
            monitor = null;
        }
    }

    public void setSnapshotInterval(int snapshotInterval) {
        this.snapshotInterval = snapshotInterval;
    }

    public void addCollector(final Collector<?> collector) {
        synchronized (collectors) {
            collectors.add(collector);
        }
    }

    public void removeCollector(final Collector<?> collector) {
        synchronized (collectors) {
            collectors.remove(collector);
        }
    }

    public void addObserver(final GenericObserver observer) {
        checkWatchdog();

        synchronized (observers) {
            observers.add(observer);
        }

        updateObserver(observer);        
    }

    public void removeObserver(final GenericObserver observer) {
        synchronized (observers) {
            observers.remove(observer);
        }
    }

    public void collect() {
        final Collection result = new ArrayList();

        synchronized (collectors) {
            for (final Collector collector : collectors) {
                if (collector.getPolicy() != Collector.Policy.Auto) {
                    result.add(collector.collect());
                }
            }
        }

        synchronized (observers) {
            lastSnapshot = Collections.unmodifiableCollection(result);
        }
    }

    public void update() {
        synchronized (observers) {
            for (final GenericObserver observer : observers) {
                observer.update(lastSnapshot);
            }
        }
    }

    private void checkWatchdog() {
        if (monitor == null) {
            monitor = new MonitorThread();
            monitor.start();
        }
    }

    private void updateObserver(final GenericObserver observer) {
        synchronized (observers) {
            if (lastSnapshot != null && lastSnapshot.size() > 0) {
                observer.update(lastSnapshot);
            }
        }
    }

    private class MonitorThread extends Thread {
        private boolean stopped = false;

        public void run() {
            while (!stopped) {
                try {
                    collect();
                    update();

                    sleep(snapshotInterval);
                } catch (InterruptedException ignore) {}
            }
        }

        public void terminate() {
            stopped = true;
        }
    }
}