/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.monitor;

import org.asylum.configuration.Configurable;
import org.asylum.ui.UIUtil;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;

/**
 * Author: adrian.libotean
 * Date: Nov 27, 2009
 * Time: 11:50:26 AM
*/
public class MemoryObserver implements GenericObserver {
    public static final Configurable<Color> COLOR_DEFAULT = new Configurable<Color>(Color.DARK_GRAY.darker());
    public static final Configurable<Color> COLOR_WARNING = new Configurable<Color>(Color.ORANGE.darker());
    public static final Configurable<Color> COLOR_CRITICAL = new Configurable<Color>(Color.RED.darker());
    //--
    private JProgressBar progressBar;
    private AbstractButton button;

    public MemoryObserver(final JProgressBar progressBar, final AbstractButton button) {
        super();

        this.progressBar = progressBar;
        this.button = button;
    }

    public MemoryObserver(final JProgressBar progressBar) {
        this(progressBar, null);
    }

    protected JProgressBar getProgressBar() {
        return progressBar;
    }

    protected AbstractButton getButton() {
        return button;
    }

    public void update(Collection snapshot) {
        for (Object item : snapshot) {
            if (item instanceof MemoryCollector.MemoryBundle) {
                final MemoryCollector.MemoryBundle bundle = (MemoryCollector.MemoryBundle) item;

                final Runnable worker = new Runnable() {
                    public void run() {
                        refresh(bundle);
                    }
                };

                UIUtil.executeOnSwing(worker);
                
                break;
            }
        }
    }

    protected void refresh(final MemoryCollector.MemoryBundle bundle) {
        getProgressBar().setForeground(MemoryObserver.COLOR_DEFAULT.get());

        if (bundle.getOccupiedPercent() > 60) {
            getProgressBar().setForeground(MemoryObserver.COLOR_WARNING.get());
        } else if (bundle.getOccupiedPercent() > 80) {
            getProgressBar().setForeground(MemoryObserver.COLOR_CRITICAL.get());
        }

        getProgressBar().setValue( (int) bundle.getOccupiedPercent() );
    }
}