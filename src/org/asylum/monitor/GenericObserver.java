/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.monitor;

import java.util.Collection;

/**
 * Author: adrian.libotean
 * Date: Nov 27, 2009
 * Time: 11:49:45 AM
*/
public interface GenericObserver {
    void update(final Collection snapshot);
}