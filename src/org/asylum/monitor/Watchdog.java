/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.monitor;

/**
 * Author: adrian.libotean
 * Date: Nov 27, 2009
 * Time: 11:51:50 AM
 */
public interface Watchdog {
    void addCollector(Collector<?> collector);
    void removeCollector(Collector<?> collector);

    void addObserver(GenericObserver observer);
    void removeObserver(GenericObserver observer);

    void collect();
    void update();
}