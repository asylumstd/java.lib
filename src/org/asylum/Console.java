/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum;

public interface Console {
    void startUp();
    void shutdown();

    Console print(CharSequence sequence);
    Console println(CharSequence sequence);

    void flush();

    void error(final String message);
    void error(final String message, Throwable ex);
    void warning(final String message);
    void info(final String message);
}