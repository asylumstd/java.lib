/*
 * Copyright (c) 2011 Asylum Studios Romania.
 */

package org.asylum;

import org.asylum.services.TaskService;
import org.asylum.services.TaskMonitor;

import java.beans.PropertyChangeListener;

/**
 * Author: adrian.libotean
 * Date: 08/04/2011
 * Time: 12:47:28 PM
 * Summary:
 */
public class GenericTaskService implements TaskService, TaskMonitor {

    public void execute(final Task task) {
        if (task instanceof SimpleTask) {
            task.execute();
        } else {
            //-- wrap task into a simple non-cancelable task
            final SimpleTask<Void> newTask = new SimpleTask<Void>(task.getTitle()) {
                protected Void doInBackground() throws Exception {
                    task.execute();
                    return null;
                }

                public void onCanceled() {
                    task.onCanceled();
                }

                public void onFinished() {
                    task.onFinished();
                }
            };

            newTask.execute();
        }


        //To change body of implemented methods use File | Settings | File Templates.
    }

    public Iterable<Task> getTasks() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getName() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}