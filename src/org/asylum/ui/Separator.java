/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui;

import javax.swing.*;
import java.awt.*;

/**
 * Author: Adrian Libotean
 * Date: Aug 6, 2009
 * Time: 3:06:41 PM
 */
public class Separator extends JComponent {
    private final Color COLOR_SHADOW_LIGHT = new Color(180, 180, 180);
    private final Color COLOR_SHADOW_DARK = new Color(0, 0, 0);
    private final Color COLOR_HIGHLIGHT_DARK = new Color(209, 209, 209);
    private final Color COLOR_HIGHLIGHT_LIGHT = new Color(250, 250, 250);
    private static final int MARGIN_TOP = 2;
    private static final int MARGIN_BOTTOM = 2;
    //--
    protected int width;
    protected final Dimension minSize;

    public Separator(final int width) {
        super();

        this.width = width;
        this.minSize = new Dimension(width, 0);

        setOpaque(false);
    }

    public Dimension getMinimumSize() {
        return minSize;
    }

    public Dimension getMaximumSize() {
        return getMinimumSize();
    }

    public Dimension getPreferredSize() {
        return getMinimumSize();
    }

    // to-do: redirect via UIUtil
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        final int x = (getWidth() - 2) / 2;
        final int y = (getHeight() - MARGIN_TOP - MARGIN_BOTTOM) / 2;

        /*g2.setColor(Color.YELLOW);
        g2.fillRect(0, 0, getWidth(), getHeight());*/

        Paint p = new GradientPaint(x, MARGIN_TOP, COLOR_SHADOW_LIGHT, x, y, COLOR_SHADOW_DARK);
        g2.setPaint(p);
        g2.drawLine(x, MARGIN_TOP, x, y);
        p = new GradientPaint(x, y, COLOR_SHADOW_DARK, x, getHeight(), COLOR_SHADOW_LIGHT);
        g2.setPaint(p);
        g2.drawLine(x, y + 1, x, getHeight() - MARGIN_BOTTOM);

        p = new GradientPaint(x + 1, MARGIN_TOP, COLOR_HIGHLIGHT_DARK, x + 1, y, COLOR_HIGHLIGHT_LIGHT);
        g2.setPaint(p);
        g2.drawLine(x + 1, MARGIN_TOP, x + 1, y);
        p = new GradientPaint(x + 1, y, COLOR_HIGHLIGHT_LIGHT, x + 1, getHeight(), COLOR_HIGHLIGHT_DARK);
        g2.setPaint(p);
        g2.drawLine(x + 1, y + 1, x + 1, getHeight() - MARGIN_BOTTOM);
    }
}