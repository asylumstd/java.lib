/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.application;

import org.asylum.ui.UIUtil;
import org.asylum.ui.components.statusbar.SimpleStatusBar;
import org.asylum.ui.components.toolbar.SimpleToolBar;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MDIApplication extends GenericApplication {
    protected JFrame mainFrame;
    protected JDesktopPane desktop;
    protected SimpleToolBar toolbar;
    protected SimpleStatusBar statusbar;

    public MDIApplication() {
        super();

        mainFrame = new JFrame();

        mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        mainFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(final WindowEvent e) {
                exit(e);
            }
        });
    }

    public JFrame getMainFrame() {
        return mainFrame;
    }

    protected void configureWindow(final Window root) {
        getMainFrame().getContentPane().setLayout(new BorderLayout());

        initToolbar();
        initDesktop();
        initStatusBar();

        //-- apply properties
        super.configureWindow(root);
    }

    protected void initDesktop() {
        desktop = UIUtil.createDesktopPane();
        desktop.setBackground(UIUtil.getColor("Panel.background").darker());

        JComponent pnlWrapper = new JPanel(new BorderLayout());
        pnlWrapper.add(desktop, BorderLayout.CENTER);
        pnlWrapper.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        getMainFrame().getContentPane().add(pnlWrapper, BorderLayout.CENTER);
    }

    protected void initToolbar() {
        toolbar = UIUtil.createToolbar();
        getMainFrame().getContentPane().add(toolbar, BorderLayout.NORTH);
    }

    protected void initStatusBar() {
        statusbar = UIUtil.createStatusBar();
        getMainFrame().getContentPane().add(statusbar, BorderLayout.SOUTH);
    }
}