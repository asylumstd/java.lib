/*
 * Copyright (c) 2011 Asylum Studios Romania.
 */

package org.asylum.ui.application;

import javax.swing.*;
import java.util.EventObject;

/**
 * Author: adrian.libotean
 * Date: 15/04/2011
 * Time: 2:00:50 PM
 * Summary:
 */
public interface Application {
    JFrame getMainFrame();
    void exit(EventObject event);
}