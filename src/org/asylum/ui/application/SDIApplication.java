/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.application;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class SDIApplication extends GenericApplication {
    private JFrame mainFrame;

    public SDIApplication() {
        super();

        mainFrame = new JFrame();
        
        mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        mainFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(final WindowEvent e) {
                exit(e);
            }
        });
    }

    public JFrame getMainFrame() {
        return mainFrame;
    }
}