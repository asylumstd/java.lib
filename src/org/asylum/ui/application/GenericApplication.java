/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.application;

import org.asylum.*;
import org.asylum.Console;
import org.asylum.i18n.BasicPolyglot;
import org.asylum.i18n.Polyglot;
import org.asylum.configuration.Configuration;
import org.asylum.configuration.ConfigurationImpl;
import org.asylum.lang.Disposable;
import org.asylum.lang.Disposer;
import org.asylum.monitor.Watchdog;
import org.asylum.monitor.WatchdogImpl;
import org.asylum.monitor.MemoryCollector;
import org.asylum.services.ServiceHandlerNotRegisteredError;
import org.asylum.services.ServiceRegistry;
import org.asylum.services.TaskService;
import org.asylum.services.TaskMonitor;
import org.asylum.ui.GlyphService;
import org.asylum.ui.BasicGlyphService;
import org.asylum.ui.*;
import org.asylum.ui.RepaintManager;
import static org.asylum.services.ServiceRegistry.getService;
import static org.asylum.services.ServiceRegistry.register;

import javax.swing.*;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.EventObject;
import java.awt.*;
import java.awt.event.PaintEvent;
import java.util.List;
import java.util.ArrayList;

public abstract class GenericApplication implements Application, Disposable {
    protected static final Logger LOG = Logger.getLogger(GenericApplication.class.getName());
    //--
    private final List<ExitListener> exitListeners;

    protected GenericApplication() {
        super();

        //-- register services
        try {
            getService(Configuration.class);
        } catch (ServiceHandlerNotRegisteredError ex) {
            register(Configuration.class, new ConfigurationImpl());
        }

        //-- resource monitor and collectors (try to reuse if existing)
        Watchdog watchdog;
        try {
            watchdog = getService(Watchdog.class);
        } catch (ServiceHandlerNotRegisteredError ex) {
            watchdog = new WatchdogImpl();
            Disposer.register(this, ((WatchdogImpl) watchdog));
            register(Watchdog.class, watchdog);
        }
        
        watchdog.addCollector(new MemoryCollector());

        final GenericTaskService service = new GenericTaskService();
        register(TaskService.class, service);
        register(TaskMonitor.class, service);

        register(Polyglot.class, new BasicPolyglot());
        register(GlyphService.class, new BasicGlyphService());

        register(UIFactory.class, new BasicUIFactory());

        RepaintManager.setCurrentManager(new RepaintManager());

        exitListeners = new ArrayList<ExitListener>();
    }

    public void dispose() {}

    public abstract JFrame getMainFrame();

    public synchronized void addExitListener(final ExitListener listener) {
        exitListeners.add(listener);
    }

    public synchronized void removeExitListener(final ExitListener listener) {
        exitListeners.remove(listener);
    }

    protected void initialize(final Object[] args) {
        // bug: implement
    }

    protected void configureWindow(final Window wnd) {
        // bug: implement
    }

    protected void startup() {
        register(Console.class, new SystemConsole());
    }

    protected void ready() {}

    protected void shutdown() {
        Disposer.dispose(this);
    }

    private void waitForEmptyEventQueue() {
        boolean qEmpty = false;
        JPanel placeHolder = new JPanel();
        EventQueue q = Toolkit.getDefaultToolkit().getSystemEventQueue();

        while (!qEmpty) {
            final NotifyingEvent e = new NotifyingEvent(placeHolder);

            q.postEvent(e);
            synchronized (e) {
                while (!e.isDispatched()) {
                    try {
                        e.wait();
                    } catch (InterruptedException ignore) {}
                }
                
                qEmpty = e.isEventQEmpty();
            }
        }
    }

    private boolean canExit(final EventObject event) {
        for (ExitListener listener : exitListeners) {
            if (!listener.canExit(event)) {
                return false;
            }
        }

        return true;
    }

    private void notifyExit(final EventObject event) {
        for (ExitListener listener : exitListeners) {
            try {
                listener.willExit(event);
            } catch (Exception ex) {
                LOG.log(Level.WARNING, "ExitListener.willExit() failed", ex);
            }
        }
    }

    public synchronized void exit(final EventObject event) {
        if (canExit(event)) {
            try {
                notifyExit(event);
                shutdown();
            } catch (Exception ex) {
                LOG.log(Level.WARNING, "unexpected error in Application.shutdown()", ex);
            } finally {
                try {
                    System.exit(0);
                } catch (Exception ignore) {}
            }
        }
    }

    public static void launch(final Class<? extends GenericApplication> klass, final Object[] args) {
        ServiceRegistry.register(Console.class, new SystemConsole());

        final Runnable worker = new Runnable() {
            public void run() {
                try {
                    GenericApplication application = klass.newInstance();
                    ServiceRegistry.register(Application.class, application);

                    application.initialize(args);
                    application.startup();

                    final Task wait = application.new WaitForEmptyEventQueue();
                    wait.execute();
                } catch (Exception ex) {
                    LOG.log(Level.SEVERE, "Application failed to launch", ex);
                }
            }
        };

        UIUtil.executeOnSwing(worker);
    }

    private class WaitForEmptyEventQueue extends SimpleTask<Void> {
        private WaitForEmptyEventQueue() {
            super(WaitForEmptyEventQueue.class.getName());
        }

        protected Void doInBackground() {
            waitForEmptyEventQueue();
            return null;
        }

        public void onFinished() {
            ready();
        }
    }

    private static class NotifyingEvent extends PaintEvent implements ActiveEvent {
        private boolean dispatched = false;
        private boolean qEmpty = false;

        NotifyingEvent(Component c) {
            super(c, PaintEvent.UPDATE, null);
        }

        synchronized boolean isDispatched() {
            return dispatched;
        }

        synchronized boolean isEventQEmpty() {
            return qEmpty;
        }

        public void dispatch() {
            EventQueue q = Toolkit.getDefaultToolkit().getSystemEventQueue();
            synchronized (this) {
                qEmpty = (q.peekEvent() == null);
                dispatched = true;
                notifyAll();
            }
        }
    }
}