/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.application;

import javax.swing.*;

public class MDIWindow extends JInternalFrame {

    public MDIWindow() {
    }

    public MDIWindow(final String title) {
        super(title);
    }

    public MDIWindow(final String title, final boolean resizable) {
        super(title, resizable);
    }

    public MDIWindow(final String title, final boolean resizable, final boolean closable) {
        super(title, resizable, closable);
    }

    public MDIWindow(final String title, final boolean resizable, final boolean closable, final boolean maximizable) {
        super(title, resizable, closable, maximizable);
    }

    public MDIWindow(final String title, final boolean resizable, final boolean closable, final boolean maximizable,
                     final boolean iconifiable) {
        super(title, resizable, closable, maximizable, iconifiable);
    }
}