/*
 * Copyright (c) 2011 Asylum Studios Romania.
 */

package org.asylum.ui.application;

import java.util.EventObject;

/**
 * Author: adrian.libotean
 * Date: 08/04/2011
 * Time: 4:41:06 PM
 * Summary:
 */
public interface ExitListener {
    boolean canExit(EventObject event);
    void willExit(EventObject event);
}
