package org.asylum.ui;

/**
 * Author: adrian.libotean
* Date: Nov 10, 2010
* Time: 7:51:40 AM
* Summary:
*/
public enum GlyphSize {
    Small(16, 16),
    Medium(24, 24),
    Large(32, 32),
    XLarge(48, 48),
    Custom(-1, -1);

    private int width;
    private int height;

    private GlyphSize(final int w, final int h) {
        this.width = w;
        this.height = h;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getSuffix() {
        return Integer.toString(width) + "x" + Integer.toString(height);
    }
}
