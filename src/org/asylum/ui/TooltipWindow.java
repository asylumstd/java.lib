/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.*;

// important: extract validation renderers out of tooltip window that will be drawn in lightweight/heavyweight components
public class TooltipWindow extends JDialog {
    private static final Border BORDER_ICON = BorderFactory.createEmptyBorder(4, 4, 4, 2);
    private static final Border BORDER_TEXT_WITH_ICON = BorderFactory.createEmptyBorder(4, 2, 4, 4);
    private static final Border BORDER_TEXT_WITHOUT_ICON = BorderFactory.createEmptyBorder(4, 4, 4, 4);
    private static final int DELAY_AWTEVENT_HANDLER = 200;
    public static final int DELAY_NONE = -1;
    public static final int DELAY_DEFAULT = 5000;
    //
    protected Window owner;
    protected Theme theme;
    protected boolean autoHide;
    private AWTEventListener awtEventListener;
    private MouseListener mouseListener;
    private ComponentListener componentListener;
    private long timeVisibleSince;
    protected TooltipWindowPanel contentPane;
    private boolean listenersInstalled;

    public TooltipWindow(Frame owner, int x, int y, String message, Theme theme, int delay) {
        super(owner);

        this.owner = owner;
        this.theme = theme;
        this.autoHide = false;
        timeVisibleSince = 0;
        listenersInstalled = false;

        initComponents(message);
        setLocation(x, y);
        initEvents();
        spawnAutoHideTimer(delay);
    }

    public TooltipWindow(Frame owner, int x, int y, String message) {
        this(owner, x, y, message, Theme.INFO, DELAY_NONE);
    }

    public TooltipWindow(Frame owner, int x, int y, String message, Theme theme) {
        this(owner, x, y, message, theme, DELAY_NONE);
    }

    public TooltipWindow(Dialog owner, int x, int y, String message, Theme theme, int delay) {
        super(owner);

        this.owner = owner;
        this.theme = theme;

        initComponents(message);
        setLocation(x, y);
        initEvents();
        spawnAutoHideTimer(delay);
    }

    public TooltipWindow(Dialog owner, int x, int y, String message) {
        this(owner, x, y, message, Theme.INFO, DELAY_NONE);
    }

    public TooltipWindow(Dialog owner, int x, int y, String message, Theme theme) {
        this(owner, x, y, message, theme, DELAY_NONE);
    }

    public boolean isAutoHide() {
        return autoHide;
    }

    public void setAutoHide(boolean autoHide) {
        this.autoHide = autoHide;

        if (this.autoHide) {
            if (!listenersInstalled) {
                slideVisibleSince();
                installListeners();
            }
        } else {
            uninstallListeners();
        }
    }

    protected void initComponents(String message) {
        contentPane = createContentPane(theme, message);
        setContentPane(contentPane);

        setUndecorated(true);
        setFocusableWindowState(false);
    }

    protected void initEvents() {
        mouseListener = new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                hideWindow();
            }

            public void mouseEntered(MouseEvent e) {
                setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            }

            public void mouseExited(MouseEvent e) {
                setCursor(Cursor.getDefaultCursor());
            }

            public void mousePressed(MouseEvent e) {}
            public void mouseReleased(MouseEvent e) {}
        };

        awtEventListener = new AWTEventListener() {
            public void eventDispatched(AWTEvent event) {
                if (event instanceof MouseEvent) {
                    MouseEvent mouseEvent = (MouseEvent) event;

                    if (mouseEvent.getWhen() <= timeVisibleSince) {
                        return;
                    }

                    if (isAutoHide() && mouseEvent.getID() == MouseEvent.MOUSE_CLICKED &&
                            mouseEvent.getSource() != TooltipWindow.this &&
                            mouseEvent.getSource() != TooltipWindow.this.contentPane.lblIcon &&
                            mouseEvent.getSource() != TooltipWindow.this.contentPane.lblMessage) {

                        hideWindow();
                    }
                }
            }
        };

        componentListener = new ComponentListener() {
            public void componentHidden(ComponentEvent e) {
                hideWindow();
            }

            public void componentMoved(ComponentEvent e) {
                hideWindow();
            }

            public void componentResized(ComponentEvent e) {
                hideWindow();
            }

            public void componentShown(ComponentEvent e) {}
        };
    }

    private void spawnAutoHideTimer(int delay) {
        if (delay <= 0) {
            return;
        }

        Timer tmrHide = new Timer(delay, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                processWindowEvent(new WindowEvent(owner, WindowEvent.WINDOW_CLOSING));
            }
        });
        tmrHide.start();
    }

    public JLabel getLabel() {
        return contentPane.getLabel();
    }

    public ImageIcon getIcon() {
        return contentPane.getIcon();
    }

    public void setIcon(ImageIcon icon) {
        contentPane.setIcon(icon);
        validate();
    }

    protected void installListeners() {
        addMouseListener(mouseListener);

        UIUtil.executeLater(new SwingRunnable() {
            public void run() {
                Toolkit.getDefaultToolkit().addAWTEventListener(awtEventListener, AWTEvent.MOUSE_EVENT_MASK);
            }
        });

        if (owner != null) {
            owner.addComponentListener(componentListener);
        }

        listenersInstalled = true;
    }

    protected void uninstallListeners() {
        removeMouseListener(mouseListener);

        Toolkit.getDefaultToolkit().removeAWTEventListener(awtEventListener);

        if (owner != null) {
            owner.removeComponentListener(componentListener);
        }

        listenersInstalled = false;
    }

    public void setVisible(boolean b) {
        super.setVisible(b);

        if (!b && isAutoHide()) {
            uninstallListeners();
        }
    }

    protected void hideWindow() {
        uninstallListeners();
        setVisible(false);
        dispose();
    }

    protected void slideVisibleSince() {
        timeVisibleSince = System.currentTimeMillis() + DELAY_AWTEVENT_HANDLER;
    }

    public static TooltipWindowPanel createContentPane(Theme theme, ImageIcon icon, String message, JPanel content) {
        return new TooltipWindowPanel(theme, icon, message, content);
    }

    public static TooltipWindowPanel createContentPane(Theme theme, String message, JPanel content) {
        return createContentPane(theme, theme.getIcon(), message, content);
    }

    public static TooltipWindowPanel createContentPane(Theme theme, ImageIcon icon, String message) {
        return createContentPane(theme, icon, message, null);
    }

    public static TooltipWindowPanel createContentPane(Theme theme, String message) {
        return createContentPane(theme, theme.getIcon(), message);
    }

    public static TooltipWindowPanel createContentPane(Theme theme, JPanel content) {
        return createContentPane(theme, null, null, content);
    }

    public static TooltipWindowPanel createContentPane(Theme theme) {
        return createContentPane(theme, theme.getIcon(), null);
    }

    public static TooltipWindowPanel createContentPane() {
        return createContentPane(Theme.WARNING);
    }

    protected static class TooltipWindowPanel extends JPanel {
        protected JLabel lblIcon;
        protected JLabel lblMessage;
        protected ImageIcon icon;
        protected String message;
        protected Theme theme;
        protected JPanel pnlContent;

        public TooltipWindowPanel(Theme theme, ImageIcon icon, String message, JPanel content) {
            super(new BorderLayout());

            this.theme = theme;
            this.icon = icon;
            this.message = message;
            this.pnlContent = content;

            setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED, this.theme.getLight(), this.theme.getDark()));

            lblIcon = new JLabel(this.icon);
            lblIcon.setBorder(BORDER_ICON);
            lblIcon.setVerticalAlignment(SwingConstants.TOP);
            lblIcon.setOpaque(false);

            lblMessage = new JLabel(this.message, JLabel.LEFT);
            lblMessage.setBorder(BORDER_TEXT_WITH_ICON);
            lblMessage.setOpaque(false);
            lblMessage.setForeground(this.theme.getText());
            if (this.theme.getFont() != null) {
                lblMessage.setFont(this.theme.getFont());
            }

            add(lblIcon, BorderLayout.WEST);
            add(lblMessage, BorderLayout.CENTER);
            setBackground(this.theme.getBackground());

            if (this.icon == null) {
                lblIcon.setVisible(false);
                lblMessage.setBorder(BORDER_TEXT_WITHOUT_ICON);
            }

            if (this.message == null || this.message.length() <= 0) {
                lblMessage.setVisible(false);
            }

            //-- decorate content panel
            if (pnlContent != null) {
                removeAll();

                JPanel pnlTop = new JPanel(new FlowLayout(FlowLayout.LEFT, 4, 0));
                pnlTop.setOpaque(false);
                pnlTop.add(lblIcon);
                pnlTop.add(lblMessage);

                add(pnlTop, BorderLayout.NORTH);
                add(pnlContent, BorderLayout.CENTER);

                setBackground(this.theme.getBackground());
            }
        }

        public JLabel getLabel() {
            return lblMessage;
        }

        public ImageIcon getIcon() {
            return icon;
        }

        public void setIcon(ImageIcon icon) {
            this.icon = icon;
            lblIcon.setIcon(this.icon);
            lblIcon.setVisible(this.icon != null);
            validate();
        }
    }

    public enum Theme {
        INFO(Color.WHITE, Color.BLACK, new Color(183, 203, 231), new Color(51, 85, 159),
            "org/asylum/ui/resources/icons/info.png", UIUtil.getFont(Theme.class, "info")),
        WARNING(new Color(255, 250, 232), Color.BLACK, new Color(245, 235, 190), new Color(245, 221, 104),
                "org/asylum/ui/resources/icons/warning.png", UIUtil.getFont(Theme.class, "warning")),
        ERROR(Color.PINK, Color.BLACK, new Color(255, 109, 109), Color.RED, "org/asylum/ui/resources/icons/error.png",
            UIUtil.getFont(Theme.class, "error")),
        VALIDATION(new Color(255, 245, 200), Color.BLACK, Color.PINK, Color.RED,
            "org/asylum/ui/resources/icons/validation.png", UIUtil.getFont(Theme.class, "validation")),

        INFO_SMALL(Color.WHITE, Color.BLACK, new Color(183, 203, 231), new Color(51, 85, 159),
                "org/asylum/ui/resources/icons/info.png", UIUtil.getFont(Theme.class, "info_small")),
        WARNING_SMALL(new Color(255, 250, 232), Color.BLACK, new Color(245, 235, 190), new Color(245, 221, 104),
                "org/asylum/ui/resources/icons/warning.png", UIUtil.getFont(Theme.class, "warning_small")),
        ERROR_SMALL(Color.PINK, Color.BLACK, new Color(255, 109, 109), Color.RED, "org/asylum/ui/resources/icons/error.png",
                UIUtil.getFont(Theme.class, "error_small"));

        private Color background;
        private Color text;
        private Color light;
        private Color dark;
        private ImageIcon icon;
        private Font font;

        Theme(Color background, Color text, Color light, Color dark, String iconName) {
            this.background = background;
            this.text = text;
            this.light = light;
            this.dark = dark;
            this.icon = (ImageIcon) UIUtil.getIcon(this, iconName); 
        }

        Theme(Color background, Color text, Color light, Color dark, String iconName, Font font) {
            this(background, text, light, dark, iconName);
            this.font = font;
        }

        public Color getBackground() {
            return background;
        }

        public Color getText() {
            return text;
        }

        public Color getLight() {
            return light;
        }

        public Color getDark() {
            return dark;
        }

        public ImageIcon getIcon() {
            return icon;
        }

        public Font getFont() {
            return font;
        }
    }
}