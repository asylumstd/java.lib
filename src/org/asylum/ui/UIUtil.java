/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui;

import org.asylum.i18n.Localizable;
import org.asylum.i18n.Polyglot;
import static org.asylum.services.ServiceRegistry.getService;
import org.asylum.ui.components.toolbar.SimpleToolBar;
import org.asylum.ui.components.statusbar.SimpleStatusBar;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.MessageFormat;

/**
 * Author: adrian.libotean
 * Date: Dec 2, 2009
 * Time: 3:27:51 PM
 */
public class UIUtil {

    public static String getString(Localizable text) {
        return getService(Polyglot.class).i18n(text, null);
    }

    public static String getString(Localizable text, final CharSequence context) {
        return getService(Polyglot.class).i18n(text, context);
    }

    public static String getString(final Object requester, final String id, final CharSequence context) {
        return getString(Localizable.factory(requester, id), context);
    }

    public static String getString(final Object requester, final String id) {
        return getString(requester, id, null);
    }

    public static String getString(final String id) {
        return getString(null, id, null);
    }

    public static Icon getIcon(final Object requester, final String location, final GlyphSize size) {
        return getService(GlyphService.class).getGlyph(GlyphID.factory(requester, location), size);
    }

    public static Icon getIcon(final Object requester, final String location) {
        return getService(GlyphService.class).getGlyph(GlyphID.factory(requester, location));
    }

    public static Icon getIcon(final String location) {
        return getService(GlyphService.class).getGlyph(GlyphID.factory(null, location));
    }

    public static Font getFont(final Object requester, final String id) {
        final String faceName = getString(requester, id + ".font");
        return Font.decode(faceName);
    }

    public static Font getFont(final Class klass, final String id) {
        final String faceName = getString(Localizable.factory(klass, id + ".font"));
        return Font.decode(faceName);
    }

    public static Icon getLocalizedIcon(final Object requester, final String location) {
        final String translated = getString(requester, location);
        if (translated != null) {
            return getIcon(requester, translated);
        }

        return null;
    }

    public static Image getImage(final Object requester, final String location) {
        return getService(GlyphService.class).getImage(GlyphID.factory(requester, location));
    }

    public static Image getLocalizedImage(final Object requester, final String location) {
        return getImage(requester, getString(requester, location));
    }

    public static MessageFormat getMessageFormatter() {
        return getService(Polyglot.class).getMessageFormatter();
    }

    public static MessageFormat getMessageFormatter(final String pattern) {
        return getService(Polyglot.class).getMessageFormatter(pattern);
    }

    public static Action getAction(final Object requester, final String id) {
        return getAction(requester, id, requester);
    }

    public static Action getAction(final Object requester, final String id, final Object implementer) {
        Method m;
        final Class klass = implementer.getClass();
        try {
            m = klass.getMethod(id, ActionEvent.class);
        } catch (NoSuchMethodException ex) {
            try {
                m = klass.getMethod(id);
            } catch (NoSuchMethodException ex2) {
                // bug: log
                throw new IllegalStateException("Method: `" + id + "` of instance of " + implementer + " not found.");
            }
        }

        final Method method = m;
        final ActionListener adapter = new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                try {
                    if (method.getParameterTypes() != null && method.getParameterTypes().length == 1) {
                        method.invoke(implementer, e);
                    } else {
                        method.invoke(implementer);
                    }
                } catch (Exception ex) {
                    // bug: log
                    System.out.println(method);
                    ex.printStackTrace();
                }
            }
        };

        return getAction(requester, id, adapter);
    }


    public static Action getAction(final Object requester, final String id, final ActionListener handler) {
        final Icon icon = getLocalizedIcon(requester, id + ".Action.icon");
        String caption = getString(requester, id + ".Action.text");
        final Character mnemonic;
        final int idx = caption.indexOf('&');
        if (idx >= 0) {
            mnemonic = Character.valueOf(caption.charAt(idx + 1));
            caption = caption.substring(0, idx) + caption.substring(idx + 1);
        } else {
            mnemonic = null;
        }

        final AbstractAction result = new AbstractAction(caption, icon) {
            public void actionPerformed(final ActionEvent e) {
                handler.actionPerformed(e);
            }
        };

        if (mnemonic != null) {
            result.putValue(AbstractAction.MNEMONIC_KEY, Integer.valueOf( (byte) mnemonic.charValue()) );
        }

        final String tooltip = getString(requester, id + ".Action.shortDescription");
        if (tooltip != null) {
            result.putValue(AbstractAction.SHORT_DESCRIPTION, tooltip);
        }

        return result;
    }

    public static Color getColor(final String key) {
        return getService(UIFactory.class).getColor(key);
    }

    public static JComponent createToolbarSeparator() {
        return createToolbarSeparator(5);
    }

    public static JComponent createToolbarSeparator(int width) {
        return getService(UIFactory.class).createSeparator(width);
    }

    public static SimpleToolBar createToolbar() {
        return getService(UIFactory.class).createToolBar();
    }

    public static SimpleStatusBar createStatusBar() {
        return getService(UIFactory.class).createStatusBar();
    }

    public static JDesktopPane createDesktopPane() {
        return getService(UIFactory.class).createDesktopPane();
    }

    public static void executeOnSwing(final Runnable worker) {
        if (SwingUtilities.isEventDispatchThread()) {
            worker.run();
        } else {
            SwingUtilities.invokeLater(worker);
        }
    }

    public static void executeLater(final Runnable worker) {
        SwingUtilities.invokeLater(worker);
    }

    public static void executeAndWait(final Runnable worker) {
        try {
            SwingUtilities.invokeAndWait(worker);
        } catch (InterruptedException ignore) {
        } catch (InvocationTargetException ignore) {}
    }
}