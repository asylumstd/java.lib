/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui;

/**
 * Author: adrian.libotean
 * Date: Dec 2, 2009
 * Time: 3:49:41 PM
 */
public interface SwingRunnable extends Runnable {
}