package org.asylum.ui;

import javax.swing.*;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.Map;
import java.awt.*;
import java.awt.image.VolatileImage;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;

/**
 * Author: adrian.libotean
 * Date: Nov 10, 2010
 * Time: 3:05:23 PM
 * Summary:
 */
public class RepaintManager extends javax.swing.RepaintManager {
    private static final Logger LOG = Logger.getLogger(RepaintManager.class.getName());

    private Map<GraphicsConfiguration, VolatileImage> myImagesMap;
    private static final String FAULTY_FIELD_NAME = "volatileMap";

    WeakReference<JComponent> myLastComponent;

    public Image getVolatileOffscreenBuffer(Component c, int proposedWidth, int proposedHeight) {
        final Image buffer = super.getVolatileOffscreenBuffer(c, proposedWidth, proposedHeight);
        clearLeakyImages();
        return buffer;
    }

    @SuppressWarnings({"unchecked"})
    private void clearLeakyImages() {
        if (myImagesMap == null) {
            try {
                Field volMapField = javax.swing.RepaintManager.class.getDeclaredField(FAULTY_FIELD_NAME);
                volMapField.setAccessible(true);
                myImagesMap = (Map<GraphicsConfiguration, VolatileImage>) volMapField.get(this);
            }
            catch (Exception e) {
                LOG.log(Level.SEVERE, "Unable to clear leaky images.", e);
            }
        }

        if (myImagesMap.size() > 3) {
            for (VolatileImage image : myImagesMap.values()) {
                image.flush();
            }
            myImagesMap.clear();
        }
    }

    public void addInvalidComponent(final JComponent invalidComponent) {
        checkThreadViolations(invalidComponent);

        super.addInvalidComponent(invalidComponent);
    }

    public void addDirtyRegion(final JComponent c, final int x, final int y, final int w, final int h) {
        checkThreadViolations(c);

        super.addDirtyRegion(c, x, y, w, h);
    }

    private void checkThreadViolations(JComponent c) {
        if (!SwingUtilities.isEventDispatchThread() && c.isShowing()) {
            boolean repaint = false;
            boolean fromSwing = false;
            boolean imageUpdate = false;
            @SuppressWarnings({"ThrowableInstanceNeverThrown"})
            final Throwable exception = new Throwable();
            StackTraceElement[] stackTrace = exception.getStackTrace();
            for (StackTraceElement st : stackTrace) {
                if (repaint && st.getClassName().startsWith("javax.swing.")) {
                    fromSwing = true;
                }

                if (repaint && "imageUpdate".equals(st.getMethodName())) {
                    imageUpdate = true;
                }

                if ("repaint".equals(st.getMethodName())) {
                    repaint = true;
                    fromSwing = false;
                }
            }

            if (imageUpdate) {
                //assuming it is java.awt.image.ImageObserver.imageUpdate(...)
                //image was asynchronously updated, that's ok
                return;
            }

            if (repaint && !fromSwing) {
                //no problems here, since repaint() is thread safe
                return;
            }

            //ignore the last processed component
            if (myLastComponent != null && c == myLastComponent.get()) {
                return;
            }
            myLastComponent = new WeakReference<JComponent>(c);

            LOG.log(Level.SEVERE,
                "Access to realized (ever shown) UI components should be done only from the AWT event dispatch thread," +
                " revalidate(), invalidate() & repaint() is ok from any thread", exception);
        }
    }
}