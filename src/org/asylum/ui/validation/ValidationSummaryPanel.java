/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.validation;

import org.asylum.ui.TooltipWindow;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Author: adrian.libotean
 * Date: 30.05.2007
 * Time: 10:51:11
 * Summary:
 */
public class ValidationSummaryPanel extends JPanel implements ValidationSummary {
    protected static final Border BORDER_ITEM = BorderFactory.createEmptyBorder(2, 2, 2, 2);
    //
    protected Border border;
    protected Validation validation;
    protected Set<String> messages;

    public ValidationSummaryPanel(Validation validation) {
        super();

        this.validation = validation;
        this.messages = new HashSet<String>();
        this.border = BorderFactory.createEtchedBorder(EtchedBorder.RAISED, TooltipWindow.Theme.VALIDATION.getLight(),
            TooltipWindow.Theme.VALIDATION.getDark());

        setBackground(TooltipWindow.Theme.VALIDATION.getBackground());
        setForeground(TooltipWindow.Theme.VALIDATION.getText());

        hidePanel();
    }

    public void clear() {
        messages.clear();
        removeAll();
    }

    public void add(String message) {
        if (message == null) {
            return;
        }

        String trimmed = message.trim();
        if (trimmed.length() > 0) {
            messages.add(trimmed);
        }
    }

    public void finish() {
        removeAll();

        if (messages.size() > 0) {
            setLayout(new GridLayout(messages.size(), 1, 0, 0));
            setBorder(border);

            for (String message : messages) {
                JLabel lbl = new JLabel(message, validation.getIcon(), JLabel.LEFT);
                lbl.setOpaque(false);
                lbl.setBorder(BORDER_ITEM);
                add(lbl);
            }

            showPanel();
        } else {
            hidePanel();
        }

        revalidate();
    }

    protected void hidePanel() {
        setOpaque(false);
        setVisible(false);
    }

    protected void showPanel() {
        setOpaque(true);
        setVisible(true);
    }
}