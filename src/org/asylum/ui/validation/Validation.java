/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.validation;

import org.asylum.ui.TooltipWindow;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Author: adrian.libotean
 * Date: 29.05.2007
 * Time: 17:24:25
 * Summary: Simple and extendable Swing validation support.
 */
public class Validation {
    protected Object parent;
    protected final Collection<AbstractValidator> validators;
    protected final Collection<ValidationStatusListener> listeners;
    protected ImageIcon icnError;

    // to-do: create a validation service that will instantiate validator and handle validation api
    // to-do: api example: ValidationService.create(dialog)
    // to-do:              ValidationService.notEmpty(component)
    protected Validation(ImageIcon icon) {
        super();

        this.validators = new ArrayList<AbstractValidator>();
        this.icnError = (icon != null) ? icon : TooltipWindow.Theme.VALIDATION.getIcon();
        this.listeners = new ArrayList<ValidationStatusListener>();
        this.parent = null;
    }

    public Validation(JFrame frame, ImageIcon icon) {
        this(icon);

        this.parent = frame;
        checkParent();
    }

    public Validation(JFrame frame) {
        this(frame, null);

        this.parent = frame;
        checkParent();
    }

    public Validation(JDialog dialog, ImageIcon icon) {
        this(icon);

        this.parent = dialog;
        checkParent();
    }

    public Validation(JDialog dialog) {
        this(dialog, null);

        this.parent = dialog;
        checkParent();
    }

    private void checkParent() {
        if (parent instanceof ValidationStatusListener) {
            final ValidationStatusListener listener = (ValidationStatusListener) parent;

            if (!listeners.contains(listener)) {
                listeners.add(listener);
            }
        }
    }

    public void setIcon(ImageIcon icon) {
        this.icnError = icon;
    }

    public ImageIcon getIcon() {
        return icnError;
    }

    public Window getWindow() {
        return (Window) parent;
    }

    public Window getParent() {
        return (Window) parent;
    }

    public void setParent(JDialog parent) {
        this.parent = parent;
        checkParent();
    }

    public void setParent(JFrame parent) {
        this.parent = parent;
        checkParent();
    }

    public void add(AbstractValidator validator, String group) {
        synchronized (validators) {
            validators.add(validator);
            validator.setValidation(this);

            if (group != null && group.length() > 0) {
                validator.setGroup(group);
            }
        }
    }

    public void add(AbstractValidator validator) {
        add(validator, null);
    }

    public void remove(AbstractValidator validator) {
        synchronized (validators) {
            validators.remove(validator);
        }
    }

    public boolean validate(boolean stopOnFirst) {
        boolean result = true;

        for (AbstractValidator validator : validators) {
            boolean tmp = validator.verify(true, true);
            result = result && tmp;

            if (!result && stopOnFirst) {
                break;                
            }
        }

        if (result) {
            fireValidationPassed();
        } else {
            fireValidationFailed();
        }

        return result;
    }

    public String validate(final String prefix, final String suffix) {
        StringBuilder sb = new StringBuilder();

        boolean passed = true;
        for (AbstractValidator validator : validators) {
            if (!validator.verify(true, false)) {
                sb.append(prefix);
                sb.append(validator.getMessage());
                sb.append(suffix);
                passed = false;
            }
        }

        if (passed) {
            fireValidationPassed();
        } else {
            fireValidationFailed();
        }

        return sb.toString();
    }

    public boolean validate(ValidationSummary summary) {
        summary.clear();

        boolean passed = true;
        for (AbstractValidator validator : validators) {
            if (!validator.verify(true, false)) {
                summary.add(validator.getMessage());
                passed = false;
            }
        }

        summary.finish();

        if (passed) {
            fireValidationPassed();
        } else {
            fireValidationFailed();
        }

        return passed;
    }

    protected void fireValidationPassed() {
        synchronized (listeners) {
            for (ValidationStatusListener listener : listeners) {
                listener.validationPassed();
            }
        }
    }

    protected void fireValidationFailed() {
        synchronized (listeners) {
            for (ValidationStatusListener listener : listeners) {
                listener.validationFailed();
            }
        }
    }

    public void addListener(ValidationStatusListener listener) {
        synchronized (listeners) {
            listeners.add(listener);
        }
    }

    public void removeListener(ValidationStatusListener listener) {
        synchronized (listeners) {
            listeners.remove(listener);
        }
    }

    public void enableGroup(String group) {
        synchronized (validators) {
            for (AbstractValidator validator : validators) {
                if (group.equals(validator.getGroup())) {
                    validator.setEnabled(true);
                }
            }
        }
    }

    public void disableGroup(String group) {
        synchronized (validators) {
            for (AbstractValidator validator : validators) {
                if (group.equals(validator.getGroup())) {
                    validator.setEnabled(false);
                }
            }
        }
    }

    public TooltipWindow createTooltipWindow(int x, int y, JComponent comp, String message) {
        TooltipWindow dlg;

        if (parent instanceof JFrame) {
            dlg = new TooltipWindow( (JFrame) parent, x, y, message, TooltipWindow.Theme.VALIDATION);
        } else {
            dlg = new TooltipWindow( (JDialog) parent, x, y, message, TooltipWindow.Theme.VALIDATION);
        }

        return dlg;
    }
}