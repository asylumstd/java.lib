/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.validation;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

/**
 * Author: adrian.libotean
 * Date: 08.06.2007
 * Time: 15:49:00
 * Summary:
 */
public class FloatValidator extends AbstractValidator {
    protected FloatValidatorDelegate delegate;

    public FloatValidator(JComponent component, String message) {
        super(component, message);

        delegate = createDelegate(component);
    }

    protected FloatValidatorDelegate createDelegate(JComponent component) {
        if (component instanceof JTextComponent) {
            return new JTextComponentDelegate( (JTextComponent) component );
        } else if (component instanceof JComboBox) {
            return new JComboBoxDelegate( (JComboBox) component );
        } else {
            throw new IllegalArgumentException("Unsupported component specified");
        }
    }

    protected boolean validationCriteria() {
        return (delegate.getFloat() != null);
    }

    protected interface FloatValidatorDelegate {
        Double getFloat();
    }

    private class JTextComponentDelegate implements FloatValidatorDelegate {
        protected JTextComponent text;

        public JTextComponentDelegate(JTextComponent text) {
            this.text = text;
            setup();
        }

        public Double getFloat() {
            String txt = text.getText();
            try {
                return new Double(Double.parseDouble(txt));
            } catch (NumberFormatException e) {
                return null;
            }
        }

        protected void setup() {
            text.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {}
                public void keyReleased(KeyEvent e) {}

                public void keyPressed(KeyEvent e) {
                    hidePopup();
                }
            });
        }
    }

    private class JComboBoxDelegate implements FloatValidatorDelegate {
        protected JComboBox combo;

        public JComboBoxDelegate(JComboBox combo) {
            this.combo = combo;
            setup();
        }

        public Double getFloat() {
            String txt = combo.getSelectedItem().toString();
            try {
                return new Double(Double.parseDouble(txt));
            } catch (NumberFormatException e) {
                return null;
            }
        }

        protected void setup() {
            this.combo.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    hidePopup();
                }
            });
        }
    }
}