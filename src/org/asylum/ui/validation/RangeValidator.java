/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.validation;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Author: adrian.libotean
 * Date: 15.06.2007
 * Time: 15:30:41
 * Summary:
 */
public abstract class RangeValidator<T> extends AbstractValidator {
    protected static final boolean DEFAULT_STRICT = true;
    protected RangeValidatorDelegate<T> delegate;
    protected T lowerLimit;
    protected T upperLimit;
    protected boolean strict;

    public RangeValidator(JComponent component, T lowerLimit, T upperLimit, boolean strict,
                          String message) {
        super(component, message);

        this.lowerLimit = lowerLimit;
        this.upperLimit = upperLimit;
        this.strict = strict;

        delegate = createDelegate(component);
    }

    public RangeValidator(JComponent component, T lowerLimit, T upperLimit, String message) {
        this(component, lowerLimit, upperLimit, DEFAULT_STRICT, message);
    }

    protected boolean validationCriteria() {
        Comparable<T> value = convert(delegate.getValue());
        int i, j;

        if (value == null) {
            return false;
        }

        if (getLowerLimit() != null && getUpperLimit() != null) {
            try {
                i = value.compareTo(getLowerLimit());
                j = value.compareTo(getUpperLimit());
            } catch (ClassCastException ex) {
                return false;
            }

            if (!strict) {
                return (i >= 0 && j <= 0);
            } else {
                return (i > 0 & j < 0);
            }
        } else if (getLowerLimit() != null) {
            i = value.compareTo(getLowerLimit());

            if (!strict) {
                return (i >= 0);
            } else {
                return (i > 0);
            }
        } else if (getUpperLimit() != null) {
            i = value.compareTo(getUpperLimit());

            if (!strict) {
                return (i <= 0);
            } else {
                return (i < 0);
            }
        }

        return false;
    }

    protected T getLowerLimit() {
        return lowerLimit;
    }

    protected T getUpperLimit() {
        return upperLimit;
    }

    protected abstract Comparable<T> convert(final Object value);

    protected RangeValidatorDelegate createDelegate(JComponent component) {
        if (component instanceof JTextComponent) {
            return new JTextComponentDelegate( (JTextComponent) component );
        } else if (component instanceof JComboBox) {
            return new JComboBoxDelegate( (JComboBox) component );
        } else {
            throw new IllegalArgumentException("Unsupported component specified");
        }
    }

    protected interface RangeValidatorDelegate<T> {
        Comparable<T> getValue();
    }

    private class JTextComponentDelegate implements RangeValidatorDelegate<String> {
        protected JTextComponent text;

        public JTextComponentDelegate(JTextComponent text) {
            this.text = text;
            setup();
        }

        public Comparable<String> getValue() {
            return text.getText();
        }

        protected void setup() {
            text.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {}
                public void keyReleased(KeyEvent e) {}

                public void keyPressed(KeyEvent e) {
                    hidePopup();
                }
            });
        }
    }

    private class JComboBoxDelegate implements RangeValidatorDelegate<String> {
        protected JComboBox combo;

        public JComboBoxDelegate(JComboBox combo) {
            this.combo = combo;
            setup();
        }

        public Comparable<String> getValue() {
            Object item = combo.getSelectedItem();
            if (item instanceof String) {
                return (String) item;
            }

            return item.toString();
        }

        protected void setup() {
            this.combo.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    hidePopup();
                }
            });
        }
    }
}