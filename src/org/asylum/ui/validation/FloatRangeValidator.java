/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.validation;

import javax.swing.*;

/**
 * Author: adrian.libotean
 * Date: 22.06.2007
 * Time: 13:22:41
 * Summary:
 */
public class FloatRangeValidator extends RangeValidator<Double> {

    public FloatRangeValidator(JComponent component, Double lower, Double upper, boolean strict, String message) {
        super(component, lower, upper, strict, message);
    }

    public FloatRangeValidator(JComponent component, Double lower, Double upper, String message) {
        this(component, lower, upper, DEFAULT_STRICT, message);
    }

    protected Comparable<Double> convert(final Object value) {
        if (value instanceof Number) {
            final Number number = (Number) value;
            return Double.valueOf(number.doubleValue());
        } else {
            String str = value.toString();
            try {
                return Double.valueOf(str);
            } catch (NumberFormatException ex) {
                return null;
            }
        }
    }
}