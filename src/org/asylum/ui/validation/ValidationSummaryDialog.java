/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.validation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Author: adrian.libotean
 * Date: 06.06.2007
 * Time: 15:00:39
 * Summary:
 */
public class ValidationSummaryDialog extends JDialog implements ValidationSummary {
    protected Validation validation;
    protected ValidationSummaryPanel pnlSummary;
    protected JButton btnClose;
    protected boolean show = false;

    public ValidationSummaryDialog(Validation validation) {
        super( (JFrame) null, "Validation errors",  true );

        this.validation = validation;

        initLayout();
        initComponents();
        initEvents();
    }

    protected void initComponents() {
        pnlSummary = new ValidationSummaryPanel(validation);
        btnClose = new JButton("Close");

        JPanel pnlContent = new JPanel(new BorderLayout());
        pnlContent.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        pnlContent.add(pnlSummary, BorderLayout.CENTER);

        JPanel pnlButtons = new JPanel(new FlowLayout(FlowLayout.CENTER));
        pnlButtons.add(btnClose);

        add(pnlContent, BorderLayout.CENTER);
        add(pnlButtons, BorderLayout.SOUTH);
    }

    protected void initLayout() {
        setLayout(new BorderLayout());
    }

    protected void initEvents() {
        btnClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            }
        });
    }

    public void clear() {
        pnlSummary.clear();
    }

    public void add(String message) {
        pnlSummary.add(message);
        show = true;
    }

    public void finish() {
        pnlSummary.finish();

        if (show) {
            pack();
            setVisible(true);
        }
    }
}