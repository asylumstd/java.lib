/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.validation;

import javax.swing.*;

/**
 * Author: adrian.libotean
 * Date: 22.06.2007
 * Time: 13:03:53
 * Summary:
 */
public class IntegerRangeValidator extends RangeValidator<Long> {

    public IntegerRangeValidator(JComponent component, Long lower, Long upper, boolean strict, String message) {
        super(component, lower, upper, strict, message);
    }

    public IntegerRangeValidator(JComponent component, Long lower, Long upper, String message) {
        this(component, lower, upper, DEFAULT_STRICT, message);
    }

    protected Comparable<Long> convert(final Object value) {
        if (value instanceof Number) {
            final Number number = (Number) value;
            return Long.valueOf(number.longValue());
        } else {
            String str = value.toString();
            try {
                return Long.valueOf(str);
            } catch (NumberFormatException e) {
                return null;
            }
        }
    }
}