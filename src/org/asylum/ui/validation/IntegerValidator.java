/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.validation;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

/**
 * Author: adrian.libotean
 * Date: 08.06.2007
 * Time: 14:17:14
 * Summary:
 */
public class IntegerValidator extends AbstractValidator {
    protected IntegerValidatorDelegate delegate;

    public IntegerValidator(JComponent component, String message) {
        super(component, message);

        delegate = createDelegate(component);
    }

    protected IntegerValidatorDelegate createDelegate(JComponent component) {
        if (component instanceof JTextComponent) {
            return new JTextComponentDelegate( (JTextComponent) component );
        } else if (component instanceof JComboBox) {
            return new JComboBoxDelegate( (JComboBox) component );
        } else {
            throw new IllegalArgumentException("Unsupported component specified");
        }
    }

    protected boolean validationCriteria() {
        return (delegate.getInteger() != null);
    }

    protected interface IntegerValidatorDelegate {
        Integer getInteger();
    }

    private class JTextComponentDelegate implements IntegerValidatorDelegate {
        protected JTextComponent text;

        public JTextComponentDelegate(JTextComponent text) {
            this.text = text;
            setup();
        }

        public Integer getInteger() {
            String txt = text.getText();
            try {
                return Integer.valueOf(Integer.parseInt(txt));
            } catch (NumberFormatException e) {
                return null;
            }
        }

        protected void setup() {
            text.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {}
                public void keyReleased(KeyEvent e) {}

                public void keyPressed(KeyEvent e) {
                    hidePopup();
                }
            });
        }
    }

    private class JComboBoxDelegate implements IntegerValidatorDelegate {
        protected JComboBox combo;

        public JComboBoxDelegate(JComboBox combo) {
            this.combo = combo;
            setup();
        }

        public Integer getInteger() {
            String txt = combo.getSelectedItem().toString();
            try {
                return Integer.valueOf(Integer.parseInt(txt));
            } catch (NumberFormatException e) {
                return null;
            }
        }

        protected void setup() {
            this.combo.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    hidePopup();
                }
            });
        }
    }
}