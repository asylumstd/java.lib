/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.validation;

/**
 * Author: adrian.libotean
 * Date: 29.05.2007
 * Time: 17:51:02
 * Summary:
 */
public interface ValidationSummary {
    void clear();
    void add(String message);
    void finish();
}