/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.validation;

/**
 * Author: adrian.libotean
 * Date: 29.05.2007
 * Time: 15:04:40
 * Summary:
 */
public interface ValidationStatusListener {
    void validationFailed();
    void validationPassed();
}