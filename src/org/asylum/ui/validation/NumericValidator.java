/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.validation;

import javax.swing.*;

/**
 * Author: adrian.libotean
 * Date: 08.06.2007
 * Time: 14:17:50
 * Summary:
 */
public class NumericValidator extends FloatValidator {
    
    public NumericValidator(JComponent component, String message) {
        super(component, message);
    }
}