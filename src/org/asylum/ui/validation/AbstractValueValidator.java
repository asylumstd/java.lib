/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.validation;

import javax.swing.*;

/**
 * Author: adrian.libotean
 * Date: 31.05.2007
 * Time: 17:24:24
 * Summary:
 */
public abstract class AbstractValueValidator<T> extends AbstractValidator {
    protected static final Comparable INVALID = null;
    //
    protected Comparison comparison;

    public AbstractValueValidator(JComponent comp, Comparison comparison, String message) {
        super(comp, message);

        this.comparison = comparison;
    }

    protected boolean validationCriteria() {
        T konst = getConstant();
        Comparable<? super T> val = getValue();

        if (konst == INVALID || val == INVALID) {
            return false;
        }

        if (comparison == Comparison.Equals) {
            return konst.equals(val);
        } else if (comparison == Comparison.Identity) {
            return konst == val;
        } else if (comparison == Comparison.Different) {
            return !konst.equals(val);
        } else if (comparison == Comparison.Lower) {
            int c = val.compareTo(konst);
            return c < 0;
        } else if (comparison == Comparison.Greater) {
            int c = val.compareTo(konst);
            return c > 0;
        } else if (comparison == Comparison.LowerOrEqual) {
            int c = val.compareTo(konst);
            return c <= 0;
        } else if (comparison == Comparison.GreaterOrEqual) {
            int c = val.compareTo(konst);
            return c >= 0;
        } else {
            throw new IllegalArgumentException("Invalid state");
        }
    }

    protected abstract T getConstant();
    protected abstract Comparable<? super T> getValue();

    public enum Comparison {
        Identity,
        Equals,
        Different,
        Lower,
        Greater,
        LowerOrEqual,
        GreaterOrEqual
    }
}