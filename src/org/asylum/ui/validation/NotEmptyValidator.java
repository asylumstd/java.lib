/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.validation;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Author: adrian.libotean
 * Date: 29.05.2007
 * Time: 15:09:49
 * Summary
 */
public class NotEmptyValidator extends AbstractValidator {
    protected NotEmptyDelegate delegate;

    public NotEmptyValidator(JComponent component, String message) {
        super(component, message);

        delegate = createDelegate(component);
    }

    protected NotEmptyDelegate createDelegate(JComponent component) {
        if (component instanceof JTextComponent) {
            return new JTextComponentDelegate( (JTextComponent) component );
        } else if (component instanceof JComboBox) {
            return new JComboBoxDelegate( (JComboBox) component );
        } else {
            throw new IllegalArgumentException("Unsupported component specified");
        }
    }

    protected boolean validationCriteria() {
        return delegate.isValid();
    }

    protected interface NotEmptyDelegate {
        boolean isValid();
    }

    private class JTextComponentDelegate implements NotEmptyDelegate {
        protected JTextComponent textComponent;

        public JTextComponentDelegate(JTextComponent textField) {
            super();

            this.textComponent = textField;
            setup();
        }

        public boolean isValid() {
            String caption = textComponent.getText();

            return !(caption == null || caption.length() <= 0 || caption.trim().length() <= 0);
        }

        protected void setup() {
            textComponent.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {}
                public void keyReleased(KeyEvent e) {}

                public void keyPressed(KeyEvent e) {
                    hidePopup();
                    restoreComponent();
                }
            });
        }
    }

    private class JComboBoxDelegate implements NotEmptyDelegate {
        protected JComboBox combo;

        public JComboBoxDelegate(JComboBox combo) {
            super();

            this.combo = combo;
            setup();
        }

        protected void setup() {
            if (combo.isEditable()) {
                Component editor = combo.getEditor().getEditorComponent();

                if (editor instanceof JComponent) {
                    addHighlight( (JComponent) editor );
                }

                if (editor instanceof JTextComponent) {
                    JTextComponent text = (JTextComponent) editor;
                    text.addKeyListener(new KeyListener() {
                        public void keyTyped(KeyEvent e) {}

                        public void keyPressed(KeyEvent e) {
                            hidePopup();
                            restoreComponent();
                        }

                        public void keyReleased(KeyEvent e) {
                            hidePopup();
                            restoreComponent();
                        }
                    });
                }
            }

            this.combo.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    hidePopup();
                    restoreComponent();
                }
            });
        }

        public boolean isValid() {
            if (combo.isEditable()) {
                return (combo.getEditor().getItem() != null &&
                        combo.getEditor().getItem().toString().trim().length() > 0);
            }

            return (combo.getSelectedItem() != null && combo.getSelectedItem().toString().trim().length() > 0);
        }
    }
}