/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.validation;

/**
 * Author: adrian.libotean
 * Date: 29.05.2007
 * Time: 15:03:38
 * Summary:
 */

import org.asylum.ui.TooltipWindow;

import javax.swing.*;
import java.awt.*;
import java.util.HashSet;
import java.util.Set;

public abstract class AbstractValidator {
    private static final String KEY_OLD_BACKGROUND = "Validation.Background.Old";
    protected static final Color COLOR_ERROR = Color.PINK;
    //
    protected boolean enabled;
    protected Set<JComponent> highlight;
    protected JComponent component;
    protected Validation validation;
    protected String message;
    protected String group;
    protected TooltipWindow popup;

    protected AbstractValidator(JComponent c, String message) {
        super();

        validation = null;
        enabled = true;
        group = null;
        highlight = null;
        popup = null;
        component = c;
        highlight = new HashSet<JComponent>();
        highlight.add(component);
        this.message = message;

        setHighlight(component);

        initialize();
    }

    JComponent getComponent() {
        return component;
    }

    Validation getValidation() {
        return validation;
    }

    void setValidation(Validation validation) {
        this.validation = validation;
    }

    public String getMessage() {
        return message;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getGroup() {
        return group;
    }

    void setGroup(String group) {
        this.group = group;
    }

    public Set<JComponent> getHighlight() {
        return highlight;
    }

    public void setHighlight(JComponent highlight) {
        this.highlight.clear();
        addHighlight(highlight);
    }

    public void setHighlight(Set<JComponent> highlight) {
        this.highlight.clear();

        for (JComponent item : highlight) {
            this.highlight.add(item);
            item.putClientProperty(KEY_OLD_BACKGROUND, item.getBackground());
        }
    }

    public void addHighlight(JComponent component) {
        if (component == null) {
            return;
        }

        this.highlight.add(component);
        component.putClientProperty(KEY_OLD_BACKGROUND, component.getBackground());
    }

    public void removeHighlight(JComponent component) {
        this.highlight.remove(component);
    }

    protected ImageIcon getIcon() {
        if (validation != null) {
            return validation.getIcon();
        }

        return null;
    }

    protected void highlightComponent() {
        if (highlight == null) {
            return;
        }

        for (JComponent item : highlight) {
            if (item != null) {
                item.setBackground(COLOR_ERROR);
                item.repaint();
            }
        }
    }

    protected void restoreComponent() {
        if (highlight == null) {
            return;
        }

        for (JComponent item : highlight) {
            if (item != null) {
                Object oldBackground = item.getClientProperty(KEY_OLD_BACKGROUND);
                
                if (oldBackground instanceof Color) {
                    item.setBackground( (Color) oldBackground );
                } else {
                    item.setBackground(Color.WHITE);
                }
            }
        }
    }

    protected TooltipWindow createPopup(int x, int y, JComponent component, String message) {
        return validation.createTooltipWindow(x, y, component, message);
    }

    protected void showPopup() {
        if (highlight == null || highlight.size() <= 0) {
            return;
        }

        if (popup != null) {
            hidePopup();
        }

        Point point = determinePopupPosition();
        popup = createPopup(point.x, point.y, component, message);
        popup.setLocationRelativeTo(null);
        popup.pack();

        point = determinePopupPosition();
        if (point.getX() != 0) {
            popup.setLocation(point);
        }

        //-- if popup window will be off screen it will be moved
        adjustPopupPosition();

        popup.setVisible(true);
        popup.setAutoHide(true);
    }

    protected void hidePopup() {
        if (popup != null && popup.isVisible()) {
            popup.dispose();
        }

        popup = null;
    }

    public void setMessage(String message) {
        this.message = message;

        if (popup != null) {
            popup.getLabel().setText(this.message);
            popup.pack();
        }
    }

    protected Point determinePopupPosition() {
        Point point = new Point(0, 0);
        
        //-- determine component under which the popup will be displayed
        JComponent cmp = (highlight.size() == 1) ? highlight.iterator().next() : component;

        if (cmp.isShowing()) {
            point = cmp.getLocationOnScreen();
        }

        point.y += cmp.getHeight();

        return point;
    }

    protected void adjustPopupPosition() {
        Point point = popup.getLocation();

        GraphicsDevice[] devices = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
        for (GraphicsDevice device : devices) {
            GraphicsConfiguration configuration = device.getDefaultConfiguration();
            Rectangle bounds = configuration.getBounds();

            if (bounds.contains(point)) {
                int extentsX = point.x + popup.getWidth();
                int extentsY = point.y + popup.getHeight();

                if (extentsX > bounds.getX() + bounds.getWidth()) {
                    point.x = (int) (bounds.getX() + bounds.getWidth() - popup.getWidth());
                }

                if (extentsY > bounds.getY() + bounds.getHeight()) {
                    point.y = (int) (bounds.getY() + bounds.getHeight() - popup.getHeight());
                }

                popup.setLocation(point);
                break;
            }
        }
    }

    /**
     * This method is called when a component needs to be validated.
     * It should not be called directly. Do not override this method unless
     * you really want to change validation behavior. Implement
     * validationCriteria() instead.
     */
    boolean verify(boolean highlight, boolean popup) {
        if (enabled && !validationCriteria()) {

            if (highlight) {
                highlightComponent();
            }

            if (popup) {
                showPopup();
                component.requestFocus();
            }

            return false;
        }

        restoreComponent();
        hidePopup();

        return true;
    }

    boolean verify() {
        return verify(true, true);
    }

    protected void initialize() {
        //-- do nothing
    }

    /**
     * Implement the actual validation logic in this method. The method should
     * return false if data is invalid and true if it is valid. It is also possible
     * to set the popup message text with setMessage() before returning, and thus
     * customize the message text for different types of validation problems.
     *
     * @return false if data is invalid. true if it is valid.
     */
    protected abstract boolean validationCriteria();
}