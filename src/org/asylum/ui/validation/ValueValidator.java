/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.validation;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

/**
 * Author: adrian.libotean
 * Date: 08.06.2007
 * Time: 15:50:11
 * Summary:
 */
public class ValueValidator<T> extends AbstractValueValidator<T> {
    protected T value;
    protected ValueValidatorDelegate<? super T> delegate;

    public ValueValidator(JComponent component, T value, Comparison comparison, String message) {
        super(component, comparison, message);

        this.value = value;
        this.delegate = (ValueValidatorDelegate<? super T>) createDelegate(component);
    }

    public ValueValidator(JComponent component, T value, String message) {
        this(component, value, Comparison.Equals, message);
    }

    protected ValueValidatorDelegate createDelegate(JComponent component) {
        if (component instanceof JTextComponent) {
            return new JTextComponentDelegate( (JTextComponent) component );
        } else if (component instanceof JComboBox) {
            return new JComboBoxDelegate( (JComboBox) component );
        } else {
            throw new IllegalArgumentException("Unsupported component specified");
        }
    }

    protected boolean validationCriteria() {
        return getConstant().equals(getValue());
    }

    protected T getConstant() {
        return value;
    }

    protected Comparable<? super T> getValue() {
        return delegate.getValue();
    }

    protected interface ValueValidatorDelegate<T> {
        Comparable<? super T> getValue();
    }

    private class JTextComponentDelegate implements ValueValidatorDelegate<String> {
        protected JTextComponent text;

        public JTextComponentDelegate(JTextComponent text) {
            this.text = text;
            setup();
        }

        public Comparable<String> getValue() {
            return text.getText();
        }

        protected void setup() {
            text.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {}
                public void keyReleased(KeyEvent e) {}

                public void keyPressed(KeyEvent e) {
                    hidePopup();
                }
            });
        }
    }

    private class JComboBoxDelegate implements ValueValidatorDelegate<String> {
        protected JComboBox combo;

        public JComboBoxDelegate(JComboBox combo) {
            this.combo = combo;
            setup();
        }

        public Comparable<String> getValue() {
            Object item = combo.getSelectedItem();
            if (item instanceof String) {
                return (String) item;
            }

            return item.toString();
        }

        protected void setup() {
            this.combo.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    hidePopup();
                }
            });
        }
    }
}