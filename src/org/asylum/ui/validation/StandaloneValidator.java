/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.validation;

import org.asylum.ui.TooltipWindow;

import javax.swing.*;

/**
 * Author: adrian.libotean
 * Date: Oct 10, 2007
 * Time: 2:32:17 PM
 */
public abstract class StandaloneValidator extends AbstractValidator {
    protected Object parent;

    public StandaloneValidator(JFrame parent, JComponent c, String message) {
        super(c, message);

        this.parent = parent;
    }

    public StandaloneValidator(JDialog parent, JComponent c, String message) {
        super(c, message);

        this.parent = parent;
    }

    public Object getParent() {
        return parent;
    }

    public void setParent(JFrame parent) {
        this.parent = parent;
    }

    public void setParent(JDialog parent) {
        this.parent = parent;
    }

    protected TooltipWindow createPopup(int x, int y, JComponent component, String message) {
        TooltipWindow dlg;

        if (parent instanceof JFrame) {
            dlg = new TooltipWindow( (JFrame) parent, x, y, message, TooltipWindow.Theme.VALIDATION);
        } else {
            dlg = new TooltipWindow( (JDialog) parent, x, y, message, TooltipWindow.Theme.VALIDATION);
        }

        return dlg;
    }
}