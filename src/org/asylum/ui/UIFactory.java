/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui;

import org.asylum.ui.components.statusbar.SimpleStatusBar;
import org.asylum.ui.components.toolbar.SimpleToolBar;

import javax.swing.*;
import java.awt.*;

/**
 * Author: adrian.libotean
 * Date: Nov 25, 2009
 * Time: 5:08:34 PM
 */
public interface UIFactory {
    AbstractButton createButton(Action action);
    JPanel createPanel();
    SimpleToolBar createToolBar();
    SimpleStatusBar createStatusBar();
    JDesktopPane createDesktopPane();
    Color getColor(String key);
    JComponent createSeparator(int width);
}