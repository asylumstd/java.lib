package org.asylum.ui;

import org.asylum.services.Service;

import javax.swing.*;
import java.awt.*;

/**
 * Author: adrian.libotean
 * Date: Nov 9, 2010
 * Time: 2:41:40 PM
 * Summary: Handles all icon/glyph operations. Transparently resizes the icons if desired dimension not available.
 */
public interface GlyphService extends Service {
    Icon getGlyph(final GlyphID id, final GlyphSize size);
    Icon getGlyph(final GlyphID id);

    Icon getGlyph(final String id, final GlyphSize size);
    Icon getGlyph(final String id);

    Image getImage(final GlyphID id);
}