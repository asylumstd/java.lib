package org.asylum.ui;

import org.asylum.cache.ConcurrentSoftCache;
import org.asylum.cache.SoftCache;
import org.asylum.configuration.Configurable;

import javax.swing.*;
import java.io.*;
import java.awt.image.BufferedImage;
import java.awt.*;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * Author: adrian.libotean
 * Date: Nov 9, 2010
 * Time: 2:46:35 PM
 * Summary:
 */
public class BasicGlyphService implements GlyphService {
    private static final Logger LOG = Logger.getLogger(BasicGlyphService.class.getName());

    private static final Configurable<GlyphSize> DEFAULT_SIZE = new Configurable<GlyphSize>(GlyphSize.Small);

    private final SoftCache<String, Icon> CACHE;

    public BasicGlyphService() {
        CACHE = new ConcurrentSoftCache<String, Icon>() {
            public Icon generate(String key) {
                return null;
            }
        };
    }

    private Icon _getGlyph(final GlyphID id, final GlyphSize size) {
        final String key = getGlyphKey(id, size);

        Icon result = CACHE.load(key);
        if (result != null) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Cache hit: " + key);
            }

            return result;
        } else {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Cache miss: " + key);
            }

            Icon newIcon = loadIcon(id.getSourceClass(), key);
            if (newIcon != null) {
                return CACHE.store(key, newIcon);
            }

            return newIcon;
        }
    }

    public Icon getGlyph(final GlyphID id, final GlyphSize size) {
        Icon result = _getGlyph(id, size);

        //-- scale to desired width/height
        if (result == null) {
            Icon raw = _getGlyph(id, GlyphSize.Custom);
            if (raw != null) {
                BufferedImage img = new BufferedImage(raw.getIconWidth(), raw.getIconHeight(), Transparency.TRANSLUCENT);
                raw.paintIcon(null, img.getGraphics(), 0, 0);
                img.getScaledInstance(size.getWidth(), size.getHeight(), Image.SCALE_AREA_AVERAGING);

                result = CACHE.store(getGlyphKey(id, size), new ImageIcon(img));
            } else {
                LOG.log(Level.WARNING, "Missing resource: " + id);
            }
        }

        return result;
    }

    public Icon getGlyph(final GlyphID id) {
        return getGlyph(id, DEFAULT_SIZE.get());
    }

    public Icon getGlyph(String id, GlyphSize size) {
        return getGlyph(GlyphID.factory(null, id), DEFAULT_SIZE.get());
    }

    public Icon getGlyph(String id) {
        return getGlyph(id, DEFAULT_SIZE.get());
    }

    public Image getImage(final GlyphID id) {
        return loadImage(id.getSourceClass(), id.getPath());
    }

    public String getName() {
        return "BasicGlyphService";
    }

    private static String getExpandedPath(final Class klass, final String path) {
        if (klass == null) {
            return path;
        } else {
            return path.replace("~", klass.getPackage().getName().replace('.', '/') + "/resources");
        }
    }

    private static String getExpandedPath(final GlyphID id) {
        return getExpandedPath(id.getSourceClass(), id.getPath());
    }

    private static String getGlyphKey(final GlyphID id, final GlyphSize size) {
        final String actualPath = getExpandedPath(id);
        if (size != GlyphSize.Custom) {
            final File file = new File(actualPath);
            return actualPath.replace(file.getName(), size.getSuffix() + "/" + file.getName());
        } else {
            return actualPath;
        }
    }

    private static final int BUFFER_SIZE = 4096;
    private static ImageIcon loadIcon(Class klass, String name) {
        final String path = getExpandedPath(klass, name);
        
        if (LOG.isLoggable(Level.INFO)) {
            LOG.log(Level.INFO, "Loading icon from " + path);
        }

        try {
            InputStream in = klass.getClassLoader().getResourceAsStream(path);
            if (in != null) {
                byte[] buffer = new byte[BUFFER_SIZE];
                InputStream bin = new BufferedInputStream(in);
                ByteArrayOutputStream out = new ByteArrayOutputStream(BUFFER_SIZE);

                while (bin.read(buffer) > 0) {
                    out.write(buffer);
                }

                bin.close();
                out.flush();

                buffer = out.toByteArray();
                if (buffer != null && buffer.length > 0) {
                    return new ImageIcon(buffer);
                }
            }
        } catch (IOException ignore) {}

        return null;
    }

    private static Image loadImage(Class klass, String name) {
        final String path = getExpandedPath(klass, name);

        if (LOG.isLoggable(Level.INFO)) {
            LOG.log(Level.INFO, "Loading image from " + path);
        }

        try {
            InputStream in = klass.getClassLoader().getResourceAsStream(path);
            if (in != null) {
                byte[] buffer = new byte[BUFFER_SIZE];
                InputStream bin = new BufferedInputStream(in);
                ByteArrayOutputStream out = new ByteArrayOutputStream(BUFFER_SIZE);

                while (bin.read(buffer) > 0) {
                    out.write(buffer);
                }

                bin.close();
                out.flush();

                buffer = out.toByteArray();
                if (buffer != null && buffer.length > 0) {
                    ImageIcon icon = new ImageIcon(buffer);
                    BufferedImage img = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(),
                        BufferedImage.TYPE_INT_ARGB);
                    icon.paintIcon(null, img.getGraphics(), 0, 0);
                }
            }
        } catch (IOException ignore) {}

        return null;
    }
}