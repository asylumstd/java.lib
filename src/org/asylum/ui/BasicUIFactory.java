/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui;

import javax.swing.*;

import org.asylum.ui.components.statusbar.SimpleStatusBar;
import org.asylum.ui.components.toolbar.SimpleToolBar;
import org.asylum.ui.components.toolbar.ToolBar;

import java.awt.*;

public class BasicUIFactory implements UIFactory {

    public AbstractButton createButton(Action action) {
        return new JButton(action);
    }

    public JPanel createPanel() {
        return new JPanel(true);
    }

    public SimpleToolBar createToolBar() {
        return new ToolBar(false);
    }

    public SimpleStatusBar createStatusBar() {
        return new SimpleStatusBar();
    }

    public JDesktopPane createDesktopPane() {
        return new JDesktopPane();
    }

    public Color getColor(String key) {
        return UIManager.getColor(key);
    }

    public JComponent createSeparator(int width) {
        return new Separator(width);
    }
}