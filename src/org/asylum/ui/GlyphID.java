package org.asylum.ui;

/**
 * Author: adrian.libotean
 * Date: Nov 10, 2010
 * Time: 7:39:20 AM
 * Summary: Identitifies a glyph resource
 */
public class GlyphID {
    private Class klass;
    private String fullPath;

    private GlyphID(Class klass, String fullPath) {
        this.klass = klass;
        this.fullPath = fullPath;
    }

    public Class getSourceClass() {
        return klass;
    }

    public String getPath() {
        return fullPath;
    }

    public int hashCode() {
        if (klass == null) {
            return fullPath.hashCode();
        } else {
            return klass.hashCode() + fullPath.hashCode();
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj instanceof GlyphID) {
            final GlyphID candidate = (GlyphID) obj;

            if (klass == null) {
                return fullPath.equalsIgnoreCase(candidate.fullPath);
            } else {
                return (klass.equals(candidate.klass) && fullPath.equalsIgnoreCase(candidate.fullPath));
            }
        }

        return false;
    }

    public String toString() {
        return String.format("Class [%s]: %s", klass, fullPath); 
    }

    public static GlyphID factory(final Class klass, final String fullPath) {
        return new GlyphID(klass, fullPath);
    }

    public static GlyphID factory(final Object instance, final String fullPath) {
        return factory(instance.getClass(), fullPath);
    }

    public static GlyphID factory(final String fullPath) {
        return new GlyphID(null, fullPath);
    }
}