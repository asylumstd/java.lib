/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.toolbar;

import javax.swing.*;

/**
 * Author: Adrian Libotean
 * Date: Jul 31, 2009
 * Time: 10:53:13 AM
 */
public class ToolBarLabel extends JLabel {

    public ToolBarLabel(final String text) {
        super(text);
        init();
    }

    public ToolBarLabel(final Icon image) {
        super(image);
        init();
    }

    public ToolBarLabel(final JLabel label) {
        super(label.getText(), label.getIcon(), JLabel.LEFT);
        init();
    }

    public ToolBarLabel(String text, Icon icon) {
        super(text, icon, JLabel.LEFT);
        init();
    }

    protected void init() {
        setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
    }
}