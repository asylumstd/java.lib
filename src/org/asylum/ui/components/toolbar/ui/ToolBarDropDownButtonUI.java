/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.toolbar.ui;

import org.asylum.ui.components.toolbar.ToolBarDropDownButton;
import org.asylum.ui.components.toolbar.ToolBarButton;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ButtonUI;
import java.awt.*;

/**
 * Author: Adrian Libotean
 * Date: Jul 31, 2009
 * Time: 1:18:26 PM
 */
public class ToolBarDropDownButtonUI extends ToolBarButtonUI {
    static final int SPACE_LEFT = 2;
    static final int SPACE_RIGHT = 2;
    static final int ARROW_HEIGHT = 6;
    static final int ARROW_WIDTH = 6;
    public static final int ARROW_WIDTH_INCLUDING_SPACE = ARROW_WIDTH + SPACE_LEFT + SPACE_RIGHT;
    static final Color COLOR_NORMAL = Color.DARK_GRAY.brighter();
    static final Color COLOR_DISABLED = Color.LIGHT_GRAY;
    static final Color COLOR_SHADOW = Color.LIGHT_GRAY.brighter();
    //--
    private static ToolBarDropDownButtonUI toolbarDropDownButtonUI;

    private ToolBarDropDownButtonUI() {
        super();
    }

    @SuppressWarnings({"TypeMayBeWeakened"})
    static synchronized public ButtonUI createUI(ToolBarDropDownButton button) {
        // bug: properly set UI class

        if (toolbarDropDownButtonUI == null) {
            toolbarDropDownButtonUI = new ToolBarDropDownButtonUI();
        }

        return toolbarDropDownButtonUI;
    }


    @SuppressWarnings({"TypeMayBeWeakened"})
    // to-do: redirect all paint operations via UIUtil service
    public void paint(Graphics g, JComponent c) {
        ToolBarDropDownButton button = (ToolBarDropDownButton) c;

        super.paint(g, c);

        int y1 = (c.getHeight() / 2) - (ARROW_HEIGHT / 2);
        int x1 = c.getWidth() - ((EmptyBorder) button.getBorder()).getBorderInsets().right - SPACE_RIGHT - ARROW_WIDTH;

        if (button.getState() == ToolBarButton.State.Pressed && button.isEnabled()) {
            x1 += 1;
            y1 += 1;
        }

        boolean paintShadow = (button.getState() == ToolBarButton.State.Hovering ||
                button.getState() ==  ToolBarButton.State.Normal);
        Color color;
        if (button.isEnabled()) {
            color = COLOR_NORMAL;
        } else {
            color = COLOR_DISABLED;
            paintShadow = false;
        }

        //-- paint shadow
        Polygon p = new Polygon();
        if (paintShadow) {
            p.addPoint(x1 + 1, y1 + 1);
            p.addPoint(x1 + ARROW_WIDTH + 1, y1 + 1);
            p.addPoint(x1 + ARROW_WIDTH / 2 + 1, y1 + ARROW_HEIGHT + 1);

            g.setColor(COLOR_SHADOW);
            g.fillPolygon(p);
        }

        //-- paint arrow
        p.reset();
        p.addPoint(x1, y1);
        p.addPoint(x1 + ARROW_WIDTH, y1);
        p.addPoint(x1 + ARROW_WIDTH / 2, y1 + ARROW_HEIGHT);

        g.setColor(color);
        g.fillPolygon(p);
    }
}