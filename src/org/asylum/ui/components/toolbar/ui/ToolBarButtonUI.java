/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.toolbar.ui;

import com.sun.java.swing.plaf.windows.WindowsButtonUI;

import javax.swing.plaf.ButtonUI;
import javax.swing.*;
import java.awt.*;

import org.asylum.ui.components.toolbar.ToolBarButton;

public class ToolBarButtonUI extends WindowsButtonUI {
    private static ToolBarButtonUI toolbarButtonUI;
    //
    private static final Color COLOR_HOVER_LIGHT = new Color(200, 200, 200);
    private static final Color COLOR_HOVER_DARK = new Color(250, 250, 250);
    private static final Color COLOR_PRESSED_LIGHT = COLOR_HOVER_DARK;
    private static final Color COLOR_PRESSED_DARK = COLOR_HOVER_LIGHT;
    private static final Color COLOR_BORDER_NORMAL = new Color(150, 150, 150);
    private static final Color COLOR_BORDER_HOVER = Color.DARK_GRAY;

    protected ToolBarButtonUI() {}

    @SuppressWarnings({"TypeMayBeWeakened"})
    static synchronized public ButtonUI createUI(ToolBarButton button) {
        // important: properly set UI class
        if (toolbarButtonUI == null) {
            toolbarButtonUI = new ToolBarButtonUI();
            //COLOR_NORMAL = button.getBackground();
        }

        return toolbarButtonUI;
    }

    // to-do: redirect all paint operations via UIUtil service
    public void paint(Graphics g, JComponent c) {
        Graphics2D g2 = (Graphics2D) g;
        ToolBarButton button = (ToolBarButton) c;

        if (button.isEnabled()) {
            if (button.getState() == ToolBarButton.State.Normal) {
                /*Paint p = new GradientPaint(0, 0, COLOR_NORMAL_LIGHT, 0, button.getHeight(), COLOR_NORMAL_DARK);
                g2.setPaint(p);
                g.fillRect(0, 0, button.getWidth(), button.getHeight());*/
            } else if (button.getState() == ToolBarButton.State.Hovering) {
                Paint p = new GradientPaint(1, 1, COLOR_HOVER_DARK, 0, button.getHeight() - 2, COLOR_HOVER_LIGHT);
                g2.setPaint(p);
                g.fillRoundRect(1, 1, button.getWidth() - 2, button.getHeight() - 2, 5, 5);
            } else {
                Paint p = new GradientPaint(1, 1, COLOR_PRESSED_DARK, 0, button.getHeight() - 2, COLOR_PRESSED_LIGHT);
                g2.setPaint(p);
                g.fillRoundRect(1, 1, button.getWidth() - 2, button.getHeight() - 2, 5, 5);
            }

            super.paint(g, c);

            if (button.getState() != ToolBarButton.State.Normal) {
                g.setColor(button.getState() == ToolBarButton.State.Hovering ? COLOR_BORDER_HOVER : COLOR_BORDER_NORMAL);
                g.drawRoundRect(0, 0, button.getWidth() - 1, button.getHeight() - 1, 5, 5);
            }
        } else {
            /*Paint p = new GradientPaint(0, 0, COLOR_NORMAL_LIGHT, 0, button.getHeight(), COLOR_NORMAL_DARK);
            g2.setPaint(p);
            g.fillRect(0, 0, button.getWidth(), button.getHeight());*/
            
            super.paint(g, c);
        }
    }
}