/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.toolbar.ui;

import com.sun.java.swing.plaf.windows.WindowsButtonUI;

import javax.swing.plaf.ButtonUI;
import javax.swing.*;
import java.awt.*;

import org.asylum.ui.components.toolbar.ToolBarToggleButton;

/**
 * Author: Adrian Libotean
 * Date: Jul 31, 2009
 * Time: 11:06:17 AM
 */
public class ToolBarToggleButtonUI extends WindowsButtonUI {
    private static ToolBarToggleButtonUI toolbarToggleButtonUI;
    //--
    private static Color COLOR_NORMAL = Color.GRAY;
    private static final Color COLOR_HOVER_LIGHT = new Color(200, 200, 200);
    private static final Color COLOR_HOVER_DARK = new Color(250, 250, 250);
    private static final Color COLOR_PRESSED_LIGHT = COLOR_HOVER_DARK;
    private static final Color COLOR_PRESSED_DARK = COLOR_HOVER_LIGHT;
    private static final Color COLOR_BORDER_NORMAL = new Color(150, 150, 150);
    private static final Color COLOR_BORDER_HOVER = Color.DARK_GRAY;

    private ToolBarToggleButtonUI() {}

    @SuppressWarnings({"TypeMayBeWeakened"})
    static synchronized public ButtonUI createUI(ToolBarToggleButton button) {
        // bug: properly set UI class

        if (toolbarToggleButtonUI == null) {
            toolbarToggleButtonUI = new ToolBarToggleButtonUI();
            COLOR_NORMAL = button.getBackground();
        }

        return toolbarToggleButtonUI;
    }

    // to-do: redirect all paint operations via UIUtil service
    public void paint(Graphics g, JComponent c) {
        Graphics2D g2 = (Graphics2D) g;
        ToolBarToggleButton button = (ToolBarToggleButton) c;

        if (button.isEnabled()) {
            if (button.getState() == ToolBarToggleButton.State.Normal) {
                g.setColor(COLOR_NORMAL);
            } else if (button.getState() == ToolBarToggleButton.State.Hovering) {
                Paint p = new GradientPaint(1, 1, COLOR_HOVER_DARK, 0, button.getHeight() - 2, COLOR_HOVER_LIGHT);
                g2.setPaint(p);
            } else {
                Paint p = new GradientPaint(1, 1, COLOR_PRESSED_DARK, 0, button.getHeight() - 2, COLOR_PRESSED_LIGHT);
                g2.setPaint(p);
            }

            g.fillRoundRect(1, 1, button.getWidth() - 2, button.getHeight() - 2, 5, 5);
            super.paint(g, c);
            g.setColor(COLOR_NORMAL);

            if (button.getState() != ToolBarToggleButton.State.Normal) {
                g.setColor(button.getState() == ToolBarToggleButton.State.Hovering ? COLOR_BORDER_HOVER : COLOR_BORDER_NORMAL);
                g.drawRoundRect(0, 0, button.getWidth() - 1, button.getHeight() - 1, 5, 5);
            }
        } else {
            super.paint(g, c);
        }
    }
}