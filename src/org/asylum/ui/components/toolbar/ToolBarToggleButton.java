/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.toolbar;

import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

import org.asylum.ui.components.toolbar.ui.ToolBarToggleButtonUI;

/**
 * Author: Adrian Libotean
 * Date: Jul 31, 2009
 * Time: 11:03:44 AM
 */
public class ToolBarToggleButton extends JToggleButton {
    protected State state = State.Normal;

    public ToolBarToggleButton() {
        setFocusable(false);

        if (UIManager.getLookAndFeel() instanceof WindowsLookAndFeel) {
            setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
            setUI(ToolBarToggleButtonUI.createUI(this));
            addMouseListener(new ToolbarToggleButtonMouseListener());
        }

        getModel().addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                setState(isSelected() ? State.Pressed : State.Normal);
            }
        });
    }

    public State getState() {
        return state;
    }

    protected void setState(State newState) {
        State oldState = state;
        state = newState;

        if (!oldState.equals(state)) {
            repaint();
        }
    }

    public void setEnabled(boolean b) {
        super.setEnabled(b);

        if (!b) {
            setState(State.Normal);
        }
    }

    class ToolbarToggleButtonMouseListener implements MouseListener {
        public void mouseEntered(MouseEvent e) {
            setState(State.Hovering);
        }

        public void mouseExited(MouseEvent e) {
            setState(isSelected() ? State.Pressed : State.Normal);
        }

        public void mouseReleased(MouseEvent e) {
            if (e.getX() <= getWidth() && e.getY() <= getHeight()) {
                setState(State.Hovering);
            } else {
                setState(isSelected() ? State.Pressed : State.Normal);
            }
        }

        public void mousePressed(MouseEvent e) {
            setState(State.Pressed);
        }

        public void mouseClicked(MouseEvent e) {
            if (e.getX() <= getWidth() && e.getY() <= getHeight()) {
                setState(State.Hovering);
            } else {
                setState(isSelected() ? State.Pressed : State.Normal);
            }
        }
    }

    public enum State {
        Normal,
        Hovering,
        Pressed,
    }
}