/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.toolbar;

import org.asylum.ui.UIUtil;
import org.asylum.ui.components.toolbar.elements.ButtonElement;
import org.asylum.ui.components.toolbar.elements.LabelElement;
import org.asylum.ui.components.toolbar.elements.SeparatorElement;
import org.asylum.ui.components.toolbar.elements.ToolBarElement;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SimpleToolBar extends JToolBar {
    protected static final int MINIMUM_HEIGHT = 26;
    protected static final int MARGIN_TOP = 2;
    protected static final int MARGIN_BOTTOM = 2;
    protected static final Map<Class<?>, Class<? extends ToolBarElement>> elements;
    //--
    protected final java.util.List<ToolBarElement> items;
    protected final Map<ToolBarElement, Wrapper> wrappers;
    protected ToolBarButton btnExpand;
    protected JPanel pnlLeft;
    protected JPanel pnlRight;
    protected int fixedHeight = MINIMUM_HEIGHT;
    

    // important: implement a service oriented aproach for registering the mappings
    static {
        elements = new HashMap<Class<?>, Class<? extends ToolBarElement>>();
        //--
        elements.put(String.class, LabelElement.class);
        elements.put(Icon.class, LabelElement.class);
        elements.put(Action.class, ButtonElement.class);

        elements.put(JToolBar.Separator.class, SeparatorElement.class);
        elements.put(org.asylum.ui.Separator.class, SeparatorElement.class);
        elements.put(ToolBarLabel.class, LabelElement.class);
        elements.put(AbstractButton.class, ButtonElement.class);
        elements.put(JLabel.class, LabelElement.class);
    }

    public SimpleToolBar() {
        super(JToolBar.HORIZONTAL);

        
        this.items = new ArrayList<ToolBarElement>();
        this.wrappers = new HashMap<ToolBarElement, Wrapper>();
        setFloatable(false);

        initLayout();
        initComponents();
        initEvents();
    }

    public ToolBarElement addElement(final Object item) {
        return addElement(item, -1);
    }

    public JButton add(Action a) {
        return (JButton) addElement(a).getRenderer();
    }

    public Component add(Component comp) {
        return addElement(comp).getRenderer();
    }

    public Component add(Component comp, int index) {
        return addElement(comp, index).getRenderer();
    }

    public int getFixedHeight() {
        return fixedHeight;
    }

    public void setFixedHeight(int fixedHeight) {
        this.fixedHeight = Math.max(fixedHeight, MINIMUM_HEIGHT);

        setMinimumSize(new Dimension( (int) getMinimumSize().getWidth(), fixedHeight));
        setPreferredSize(getMinimumSize());
    }

    protected void initLayout() {
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(MARGIN_TOP, 0, MARGIN_BOTTOM, 0));
        setBackground(UIUtil.getColor("MenuBar.background"));

        setFixedHeight(MINIMUM_HEIGHT);
    }

    protected void initComponents() {
        btnExpand = new ToolBarButton();
        btnExpand.setText(">");
        btnExpand.setVisible(false);

        pnlLeft = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        pnlRight = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));

        JComponent pnlTmp = new JPanel(new BorderLayout());
        pnlTmp.setOpaque(false);
        pnlTmp.add(pnlLeft, BorderLayout.WEST);
        pnlTmp.add(pnlRight, BorderLayout.EAST);

        super.add(pnlTmp, BorderLayout.CENTER);
    }

    protected void initEvents() {}

    protected void rebuild() {
        if (!isShowing()) {
            return;
        }

        //-- clear all actions/items
        boolean oldState = btnExpand.isVisible();
        boolean hiddenActions = false;
        btnExpand.setVisible(false);

        final Rectangle bounds = getVisibleRect();
        synchronized (wrappers) {
            for (final Map.Entry<ToolBarElement, Wrapper> entry : wrappers.entrySet()) {
                final Component c = entry.getValue();
                // to-do: extract condition checking to dedicated method
                if (!bounds.contains(c.getX(), c.getY()) ||
                        !bounds.contains(c.getX() + c.getWidth(), c.getY() + c.getHeight())) {
                    hiddenActions = true;
                    break;
                }
            }
        }

        btnExpand.setEnabled(hiddenActions);
        btnExpand.setVisible(btnExpand.isEnabled());

        if (hiddenActions && !oldState) {
            invalidate();
            revalidate();
        } else if (!hiddenActions && oldState) {
            invalidate();
            revalidate();
        }
    }

    protected void expandToolbar() {
        final Window ancestor = SwingUtilities.getWindowAncestor(this);
        final JDialog floater = new JDialog(ancestor);
        floater.setUndecorated(true);
        floater.setModal(false);
        floater.getContentPane().setLayout(new FlowLayout(FlowLayout.LEFT, 1, 1));
        floater.setAlwaysOnTop(true);
        floater.setFocusable(true);
        floater.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        final JComponent pnlContent = (JComponent) floater.getContentPane();
        pnlContent.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));

        //-- compute location
        final Point location = SimpleToolBar.this.getLocation();
        SwingUtilities.convertPointToScreen(location, SimpleToolBar.this);
        floater.setLocation(location);

        synchronized (items) {
            for (final ToolBarElement element : items) {
                final ToolBarElement newElement = createElement(element.getSource());
                pnlContent.add(newElement.getRenderer());
            }
        }

        floater.pack();
        floater.setVisible(true);

        final Rectangle bounds = floater.getBounds();
        final AWTEventListener awtListener = new AWTEventListener() {
            public void eventDispatched(AWTEvent event) {
                if (event instanceof MouseEvent) {
                    final MouseEvent mouseEvent = (MouseEvent) event;
                    final Point location = mouseEvent.getLocationOnScreen();

                    if (!bounds.contains(location)) {
                        getToolkit().removeAWTEventListener(this);
                        floater.setVisible(false);
                    }
                }
            }
        };
        
        getToolkit().addAWTEventListener(awtListener, AWTEvent.MOUSE_MOTION_EVENT_MASK);

        floater.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                getToolkit().removeAWTEventListener(awtListener);
                floater.setVisible(false);
            }
        });
    }

    protected static ToolBarElement createElement(final Object item) {
        Class<? extends ToolBarElement> klass;

        synchronized (elements) {
            //-- search for exact class
            klass = elements.get(item.getClass());

            //-- search for a base class
            if (klass == null) {
                for (Map.Entry<Class<?>, Class<? extends ToolBarElement>> entry : elements.entrySet()) {
                    if (entry.getKey().isAssignableFrom(item.getClass())) {
                        klass = entry.getValue();
                        break;
                    }
                }
            }
        }

        if (klass != null) {
            try {
                //-- search for a specialized constructor
                Constructor constructor = null;
                try {
                    constructor = klass.getConstructor(item.getClass());
                } catch (NoSuchMethodException ignore) {}

                //-- search for a compatible constructor
                if (constructor == null) {
                    for (Constructor candidate : klass.getConstructors()) {
                        if (candidate.getParameterTypes().length == 1) {
                            if (candidate.getParameterTypes()[0].isAssignableFrom(item.getClass())) {
                                constructor = candidate;
                                break;
                            }
                        }
                    }
                }

                if (constructor != null) {
                    return (ToolBarElement) constructor.newInstance(item);
                } else {
                    throw new IllegalArgumentException("Could not find appropriate constructor for item: " + item);
                }
            } catch (InvocationTargetException ex) {
                throw new IllegalArgumentException("Could not find appropriate constructor for item.", ex);
            } catch (IllegalAccessException ex) {
                throw new IllegalArgumentException("Could not find appropriate constructor for item.", ex);
            } catch (InstantiationException ex) {
                throw new IllegalArgumentException("Could not find appropriate constructor for item.", ex);
            }
        } else {
            throw new IllegalArgumentException("Element class not supported. There is no renderer associated: " + item);
        }
    }

    protected ToolBarElement addElement(final Object item, final int index) {
        final ToolBarElement element = createElement(item);

        if (element.getPlacement() == ToolBarElement.PLACEMENT_LEFT) {
            pnlLeft.add(getWrapper(element), index);
        } else {
            pnlRight.add(getWrapper(element), index);
        }

        synchronized (items) {
            items.add(element);
        }

        return element;
    }

    protected Wrapper getWrapper(final ToolBarElement element) {
        Wrapper result;
        synchronized (wrappers) {
            result = wrappers.get(element);

            if (result == null) {
                result = new Wrapper(this, element);
                wrappers.put(element, result);
            }
        }

        return result;
    }
}