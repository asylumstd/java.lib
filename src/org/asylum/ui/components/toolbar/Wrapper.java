package org.asylum.ui.components.toolbar;

import org.asylum.ui.components.toolbar.elements.ToolBarElement;

import javax.swing.*;
import java.awt.*;

/**
 * Author: adrian.libotean
* Date: Nov 18, 2010
* Time: 3:31:40 PM
* Summary:
*/

class Wrapper extends JPanel {
    private final SimpleToolBar toolBar;

    Wrapper(final SimpleToolBar toolBar, final ToolBarElement element) {
        this.toolBar = toolBar;

        setLayout(new BorderLayout());
        add(element.getRenderer(), BorderLayout.CENTER);
    }

    public Dimension getMinimumSize() {
        return new Dimension( (int) super.getMinimumSize().getWidth(), toolBar.getFixedHeight() -
            SimpleToolBar.MARGIN_TOP - SimpleToolBar.MARGIN_BOTTOM);
    }

    public Dimension getPreferredSize() {
        return new Dimension( (int) super.getPreferredSize().getWidth(), toolBar.getFixedHeight() -
            SimpleToolBar.MARGIN_TOP - SimpleToolBar.MARGIN_BOTTOM);
    }

    public Dimension getMaximumSize() {
        return new Dimension( (int) super.getMaximumSize().getWidth(), toolBar.getFixedHeight() - 
            SimpleToolBar.MARGIN_TOP - SimpleToolBar.MARGIN_BOTTOM);
    }
}