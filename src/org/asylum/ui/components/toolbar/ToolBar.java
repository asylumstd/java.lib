package org.asylum.ui.components.toolbar;

import org.asylum.ui.UIUtil;
import org.asylum.ui.SwingRunnable;

import javax.swing.event.AncestorListener;
import javax.swing.event.AncestorEvent;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.HierarchyBoundsListener;
import java.awt.event.HierarchyEvent;

/**
 * Author: adrian.libotean
 * Date: Nov 18, 2010
 * Time: 3:25:59 PM
 * Summary:
 */
public class ToolBar extends SimpleToolBar {
    protected boolean expandable = true;

    public ToolBar(boolean expandable) {
        super();

        this.expandable = expandable;
    }

    public boolean isExpandable() {
        return expandable;
    }

    protected void initComponents() {
        super.initComponents();

        if (isExpandable()) {
            super.add(btnExpand, BorderLayout.EAST);
        }
    }

    protected void initEvents() {
        super.initEvents();

        if (isExpandable()) {
            btnExpand.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    expandToolbar();
                }
            });

            addAncestorListener(new AncestorListener() {
                public void ancestorAdded(AncestorEvent event) {
                    removeAncestorListener(this);

                    UIUtil.executeLater(new SwingRunnable() {
                        public void run() {
                            rebuild();
                        }
                    });
                }

                public void ancestorRemoved(AncestorEvent event) {}
                public void ancestorMoved(AncestorEvent event) {}
            });

            addHierarchyBoundsListener(new HierarchyBoundsListener() {
                public void ancestorResized(HierarchyEvent e) {
                    rebuild();
                }

                public void ancestorMoved(HierarchyEvent e) {}
            });
        }
    }
}