/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.toolbar;

import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;

import javax.swing.*;
import java.awt.*;

import org.asylum.ui.components.toolbar.ui.ToolBarDropDownButtonUI;

/**
 * Author: Adrian Libotean
 * Date: Jul 31, 2009
 * Time: 1:12:07 PM
 */
public class ToolBarDropDownButton extends ToolBarButton {

    public ToolBarDropDownButton() {
        super();

        if (UIManager.getLookAndFeel() instanceof WindowsLookAndFeel) {
            setUI(ToolBarDropDownButtonUI.createUI(this));
            setHorizontalAlignment(LEFT);
        }
    }

    public Dimension getPreferredSize() {
        Dimension old = super.getPreferredSize();
        return new Dimension((int) old.getWidth() + ToolBarDropDownButtonUI.ARROW_WIDTH_INCLUDING_SPACE,
                (int) old.getHeight());
    }

    public Dimension getMinimumSize() {
        Dimension old = super.getMinimumSize();
        return new Dimension((int) old.getWidth() + ToolBarDropDownButtonUI.ARROW_WIDTH_INCLUDING_SPACE,
                (int) old.getHeight());
    }

    public Dimension getMaximumSize() {
        Dimension old = super.getMaximumSize();
        return new Dimension((int) old.getWidth() + ToolBarDropDownButtonUI.ARROW_WIDTH_INCLUDING_SPACE,
                (int) old.getHeight());
    }
}