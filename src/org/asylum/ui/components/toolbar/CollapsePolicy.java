package org.asylum.ui.components.toolbar;

/**
 * Author: adrian.libotean
 * Date: Nov 18, 2010
 * Time: 3:26:52 PM
 * Summary:
 */
public enum CollapsePolicy {
    AlwaysVisible,
    Collapse,
    Wrap,
}