/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.toolbar;

import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;

import javax.swing.*;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

import org.asylum.ui.components.toolbar.ui.ToolBarButtonUI;

public class ToolBarButton extends JButton {
    protected State state = State.Normal;

    public ToolBarButton() {
        super();

        setFocusable(false);

        if (UIManager.getLookAndFeel() instanceof WindowsLookAndFeel) {
            setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
            setUI(ToolBarButtonUI.createUI(this));
            addMouseListener(new ToolbarButtonMouseListener());
        }
    }

    public State getState() {
        return state;
    }

    protected void setState(State newState) {
        State oldState = state;
        state = newState;

        if (!oldState.equals(state)) {
            repaint();
        }
    }

    public void setEnabled(boolean b) {
        super.setEnabled(b);

        if (!b) {
            setState(State.Normal);
        }
    }

    class ToolbarButtonMouseListener implements MouseListener {
        public void mouseEntered(MouseEvent e) {
            setState(State.Hovering);
        }

        public void mouseExited(MouseEvent e) {
            setState(State.Normal);
        }

        public void mouseReleased(MouseEvent e) {
            if (e.getX() <= getWidth() && e.getY() <= getHeight()) {
                setState(State.Hovering);
            } else {
                setState(State.Normal);
            }
        }

        public void mousePressed(MouseEvent e) {
            setState(State.Pressed);
        }

        public void mouseClicked(MouseEvent e) {
            if (e.getX() <= getWidth() && e.getY() <= getHeight()) {
                setState(State.Hovering);
            } else {
                setState(State.Normal);
            }
        }
    }

    public enum State {
        Normal,
        Hovering,
        Pressed,
    }
}