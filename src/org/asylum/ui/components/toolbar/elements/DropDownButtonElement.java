/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.toolbar.elements;

/**
 * Author: Adrian Libotean
 * Date: Aug 6, 2009
 * Time: 10:23:46 AM
 */
public class DropDownButtonElement extends AbstractElement {

    public DropDownButtonElement(byte placement, byte separator) {
        super(placement, separator);

        initRenderer();
    }

    protected void initRenderer() {
    }

    public Object getSource() {
        return null;
    }
}
