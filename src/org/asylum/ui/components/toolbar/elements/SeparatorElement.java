/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.toolbar.elements;

import org.asylum.ui.Separator;

import javax.swing.*;

/**
 * Author: Adrian Libotean
 * Date: Aug 6, 2009
 * Time: 11:02:28 AM
 */
public class SeparatorElement extends AbstractElement {
    protected Object separator;

    public SeparatorElement(final JToolBar.Separator separator) {
        super(PLACEMENT_LEFT, SEPARATOR_NONE);

        this.separator = separator;
        initRenderer();
    }

    public SeparatorElement(final Separator separator) {
        super(PLACEMENT_LEFT, SEPARATOR_NONE);

        this.separator = separator;
        initRenderer();
    }

    protected void initRenderer() {
        renderer = new Separator(7);
    }

    public Object getSource() {
        return separator;
    }
}