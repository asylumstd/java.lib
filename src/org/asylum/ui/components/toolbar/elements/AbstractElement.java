/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.toolbar.elements;

import org.asylum.ui.components.toolbar.CollapsePolicy;

import javax.swing.*;
import java.awt.*;

/**
 * Author: Adrian Libotean
 * Date: Aug 6, 2009
 * Time: 10:13:09 AM
 */
public abstract class AbstractElement<T> implements ToolBarElement<T> {
    protected final byte placement;
    protected final byte separator;
    protected JComponent renderer;

    public AbstractElement(byte placement, byte separator) {
        this.renderer = null;
        this.placement = placement;
        this.separator = separator;
    }

    public Component getRenderer() {
        return renderer;
    }

    public byte getPlacement() {
        return placement;
    }

    public byte getSeparator() {
        return separator;
    }

    public byte getPriority() {
        return 0;
    }

    public CollapsePolicy getCollapsePolicy() {
        return CollapsePolicy.AlwaysVisible;
    }

    protected abstract void initRenderer();
    public abstract T getSource();
}