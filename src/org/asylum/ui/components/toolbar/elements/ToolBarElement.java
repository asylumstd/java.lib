/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.toolbar.elements;

import org.asylum.ui.components.toolbar.CollapsePolicy;

import java.awt.*;

/**
 * Author: Adrian Libotean
 * Date: Aug 6, 2009
 * Time: 10:08:25 AM
 */
public interface ToolBarElement<T> {
    final static byte PLACEMENT_LEFT = 0;
    final static byte PLACEMENT_RIGHT = 1;

    final static byte SEPARATOR_NONE = 0;
    final static byte SEPARATOR_LEFT = 1;
    final static byte SEPARATOR_RIGHT = 2;
    final static byte SEPARATOR_BOTH = 3;

    Component getRenderer();
    byte getPlacement();
    byte getSeparator();

    T getSource();

    byte getPriority();
    CollapsePolicy getCollapsePolicy();
}