/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.toolbar.elements;

import org.asylum.ui.components.toolbar.ToolBarButton;

import javax.swing.*;

/**
 * Author: Adrian Libotean
 * Date: Aug 6, 2009
 * Time: 10:21:14 AM
 */
public class ButtonElement extends AbstractElement<Action> {
    final Action action;

    public ButtonElement(final Action action) {
        super(PLACEMENT_LEFT, SEPARATOR_NONE);

        this.action = action;
        initRenderer();
    }

    public ButtonElement(final AbstractButton button) {
        this(button.getAction());
    }

    protected void initRenderer() {
        AbstractButton button = new ToolBarButton();
        button.setAction(action);
        renderer = button;
    }

    public Action getSource() {
        return action;
    }
}