/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.toolbar.elements;

import org.asylum.ui.components.toolbar.ToolBarLabel;

/**
 * Author: Adrian Libotean
 * Date: Aug 6, 2009
 * Time: 10:24:23 AM
 */
public class LabelElement extends AbstractElement<String> {
    protected ToolBarLabel label;

    @SuppressWarnings({"TypeMayBeWeakened"})
    public LabelElement(final ToolBarLabel label) {
        super(PLACEMENT_LEFT, SEPARATOR_NONE);

        this.label = label;
        renderer = label;
    }

    protected void initRenderer() {
    }

    public String getSource() {
        return label.getText(); 
    }
}