/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.statusbar;

import org.asylum.ui.UIUtil;
import org.asylum.ui.components.toolbar.ToolBarDropDownButton;
import org.asylum.services.TaskMonitor;
import org.asylum.Task;
import org.asylum.CollectionsUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;

/**
 * Author: Adrian Libotean
 * Date: Aug 4, 2009
 * Time: 10:18:22 AM
 */
public class BackgroundActionsColumn extends GenericColumn {
    protected TaskMonitor taskMonitor;
    protected JLabel icnProgress;
    protected ToolBarDropDownButton btnTasks;
    protected JPopupMenu mnuTasks;
    protected UpdateHandler handler;
    protected TextColumn colText;

    public BackgroundActionsColumn(final MultiColumnStatusBar statusBar) {
        super(statusBar, PLACEMENT_LEFTMOST, BORDER_NONE, SEPARATOR_NONE);

        this.handler = new UpdateHandler();
        colText = new TextColumn(getStatusBar());
    }

    protected void initComponents() {
        icnProgress = new JLabel(UIUtil.getString(this, "ready.text"),
            UIUtil.getIcon(this, "~/icons/gc.png"), JLabel.LEFT);
        icnProgress.setVerticalAlignment(JLabel.CENTER);
        icnProgress.setHorizontalTextPosition(JLabel.RIGHT);
        icnProgress.setVisible(true);

        mnuTasks = new JPopupMenu();
        btnTasks = new ToolBarDropDownButton();
        btnTasks.setIcon(icnProgress.getIcon());
        btnTasks.setText(icnProgress.getText());
        btnTasks.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showTasksMenu();
            }
        });
        btnTasks.setVisible(false);

        renderer.add(icnProgress, BorderLayout.WEST);
        renderer.add(btnTasks, BorderLayout.EAST);
    }

    public void setText(final String text) {
        colText.setText(text);
    }

    public void setTaskMonitor(final TaskMonitor taskMonitor) {
        if (this.taskMonitor != null) {
            uninstall();
        }

        this.taskMonitor = taskMonitor;

        if (this.taskMonitor != null) {
            install();
        }
    }

    public TextColumn getTextColumn() {
        return colText;
    }

    protected void install() {
        this.taskMonitor.addPropertyChangeListener(handler);
    }

    protected void uninstall() {
        this.taskMonitor.removePropertyChangeListener(handler);
    }

    protected void showMonitor(final Iterable<Task> tasks) {
        final Collection<Task> items = CollectionsUtil.toCollection(tasks);

        final Runnable worker = new Runnable() {
            public void run() {
                //-- show either menu or label
                if (items.size() > 1) {
                    icnProgress.setVisible(false);
                    btnTasks.setVisible(true);
                } else {
                    icnProgress.setVisible(true);
                    btnTasks.setVisible(false);
                }

                //-- configure columns
                getStatusBar().removeColumn(colText);
                getStatusBar().addColumn(BackgroundActionsColumn.this);
                getStatusBar().revalidate();

                //-- generate tasks menu
                mnuTasks.removeAll();
                if (items.size() > 1) {
                    for (final Task task : tasks) {
                        // to-do: add support to view background tasks progress
                        JMenuItem item = new JMenuItem(task.getTitle(), btnTasks.getIcon());
                        item.addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                task.cancel(true);
                            }
                        });

                        mnuTasks.add(item);
                    }
                } else {
                    if (mnuTasks.isShowing()) {
                        mnuTasks.setVisible(false);
                    }
                }
            }
        };

        UIUtil.executeOnSwing(worker);
    }

    protected void hideMonitor() {
        getStatusBar().removeColumn(this);
        getStatusBar().addColumn(colText);
        getStatusBar().revalidate();
    }

    protected void updateTasks() {
        if (taskMonitor.getTasks() != null) {
            showMonitor(taskMonitor.getTasks());
        } else {
            hideMonitor();
        }
    }

    protected void updateForeground(final Task task) {
        if (task != null) {
            Runnable worker = new Runnable() {
                public void run() {
                    updateText(icnProgress, task.getTitle());
                    updateText(btnTasks, task.getTitle());
                }
            };

            UIUtil.executeOnSwing(worker);
        }
    }

    protected void updateText(final JComponent c, final String text) {
        if (c instanceof JLabel) {
            ((JLabel) c).setText(text);
        } else if (c instanceof AbstractButton) {
            ((AbstractButton) c).setText(text);
        }

        c.invalidate();
        c.repaint(0, 0, c.getWidth(), c.getHeight());
    }

    protected void showTasksMenu() {
        Runnable worker = new Runnable() {
            public void run() {
                mnuTasks.show(btnTasks, 0, btnTasks.getHeight() + 1);
            }
        };

        UIUtil.executeOnSwing(worker);
    }

    protected class UpdateHandler implements PropertyChangeListener {
        public void propertyChange(PropertyChangeEvent evt) {
            if (TaskMonitor.PROPERTY_TASKS.equals(evt.getPropertyName())) {
                updateTasks();
            } else if (TaskMonitor.PROPERTY_FOREGROUND_TASK.equals(evt.getPropertyName())) {
                final Task task = (Task) evt.getNewValue();
                updateForeground(task);
            }
        }
    }
}