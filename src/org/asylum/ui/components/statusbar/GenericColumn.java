/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.statusbar;

import javax.swing.*;
import java.awt.*;

/**
 * Author: Adrian Libotean
 * Date: Aug 4, 2009
 * Time: 10:59:11 AM
 */
public abstract class GenericColumn implements StatusBarColumn {
    protected MultiColumnStatusBar statusBar;
    private byte placement;
    private byte border;
    private byte separator;
    protected JPanel renderer;
    private int width;

    protected GenericColumn(final MultiColumnStatusBar statusBar, byte placement, byte border, byte separator) {
        this.statusBar = statusBar;
        this.placement = placement;
        this.border = border;
        this.separator = separator;

        width = 0;
        renderer = new JPanel(new BorderLayout());
        initComponents();
    }

    protected GenericColumn(final MultiColumnStatusBar statusBar) {
        this(statusBar, PLACEMENT_LEFT, BORDER_NONE, SEPARATOR_NONE);
    }

    public byte getPlacement() {
        return placement;
    }

    public byte getBorderType() {
        return border;
    }

    public byte getSeparator() {
        return separator;
    }

    public JPanel getRenderer() {
        return renderer;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public MultiColumnStatusBar getStatusBar() {
        return statusBar;
    }

    protected abstract void initComponents();
}