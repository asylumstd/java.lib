/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.statusbar;

import org.asylum.ui.UIUtil;

import javax.swing.*;
import java.awt.*;

/**
 * Author: Adrian Libotean
 * Date: Aug 4, 2009
 * Time: 10:58:51 AM
 */
public class TextColumn extends GenericColumn {
    protected JLabel lblText;

    public TextColumn(final MultiColumnStatusBar statusBar, byte placement, byte border, byte separator) {
        super(statusBar, placement, border, separator);
    }

    public TextColumn(final MultiColumnStatusBar statusBar) {
        super(statusBar);
    }

    protected void initComponents() {
        lblText = new JLabel("");
        lblText.setHorizontalAlignment(JLabel.LEFT);
        lblText.setVerticalAlignment(JLabel.CENTER);

        renderer.add(lblText, BorderLayout.CENTER);
    }

    public void setText(final String text) {
        Runnable worker = new Runnable() {
            public void run() {
                lblText.setText(text);
                lblText.invalidate();
                lblText.repaint(0, 0, lblText.getWidth(), lblText.getHeight());
            }
        };

        UIUtil.executeOnSwing(worker);
    }
}