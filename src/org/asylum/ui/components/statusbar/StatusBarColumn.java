/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.statusbar;

import javax.swing.*;

/**
 * Author: Adrian Libotean
 * Date: Aug 4, 2009
 * Time: 9:13:58 AM
 * Summary: Genric toolbar column
 */
public interface StatusBarColumn {
    static final byte SEPARATOR_NONE = 0;
    static final byte SEPARATOR_BOTH = 1;
    static final byte SEPARATOR_LEFT = 2;
    static final byte SEPARATOR_RIGHT = 3;

    static final byte BORDER_NONE = 0;
    static final byte BORDER_SUNKEN = 1;
    static final byte BORDER_RAISED = 2;
    public static final byte BORDER_SIMPLE = 3;

    static final byte PLACEMENT_LEFT = 0;
    static final byte PLACEMENT_CENTER = 1;
    static final byte PLACEMENT_RIGHT = 2;
    static final byte PLACEMENT_LEFTMOST = 3;
    static final byte PLACEMENT_RIGHTMOST = 4;

    byte getSeparator();
    byte getBorderType();
    byte getPlacement();
    JComponent getRenderer();
    int getWidth();
}