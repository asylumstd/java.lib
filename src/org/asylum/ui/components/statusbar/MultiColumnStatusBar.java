/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.statusbar;

import org.asylum.lang.Disposer;
import org.asylum.ui.BasicUIFactory;
import org.asylum.ui.UIUtil;
import org.asylum.lang.Disposable;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Author: Adrian Libotean
 * Date: Aug 4, 2009
 * Time: 9:13:14 AM
 * Summary: A generic multi-column toolbar
 */
public class MultiColumnStatusBar extends SimpleStatusBar implements Disposable {
    private static final int FIXED_HEIGHT = 24;
    private static final Border BORDER_NONE = BorderFactory.createEmptyBorder();
    private static final Border BORDER_SUNKEN = BorderFactory.createBevelBorder(BevelBorder.LOWERED);
    private static final Border BORDER_RAISED = BorderFactory.createBevelBorder(BevelBorder.RAISED);
    private static final Border BORDER_SIMPLE = BorderFactory.createBevelBorder(BevelBorder.LOWERED,
            Color.WHITE, UIUtil.getColor("MenuBar.background"), Color.DARK_GRAY,
            UIUtil.getColor("MenuBar.background")
    );

    //--
    protected JPanel pnlLeftMost;
    protected JPanel pnlRightMost;
    protected JPanel pnlLeft;
    protected JPanel pnlCenter;
    protected JPanel pnlRight;
    protected final Collection<StatusBarColumn> columns;
    protected final Map<StatusBarColumn, JComponent> wrappers;
    //--
    protected BackgroundActionsColumn colTasks;
    protected MemoryColumn colMemory;

    public MultiColumnStatusBar() {
        super();

        columns = new ArrayList<StatusBarColumn>();
        wrappers = new HashMap<StatusBarColumn, JComponent>();

        colTasks = new BackgroundActionsColumn(this);
        //colText.setWidth(240);
        colMemory = new MemoryColumn(this);
        Disposer.register(this, colMemory);
        
        initLayout();
        initComponents();


        addColumn(colTasks.getTextColumn());
        addColumn(colMemory);
    }

    protected void initLayout() {
        super.initLayout();

        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(4, 1, 1, 1));

        setMinimumSize(new Dimension( (int) getMinimumSize().getWidth(), FIXED_HEIGHT ));
        setPreferredSize(getMinimumSize());
    }

    protected void initComponents() {
        super.initComponents();

        pnlLeftMost = new JPanel(new BorderLayout());
        pnlRightMost = new JPanel(new BorderLayout());

        pnlLeft = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        pnlRight = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));
        pnlCenter = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));

        Container pnlTmp = new JPanel(new BorderLayout());
        pnlTmp.add(pnlLeftMost, BorderLayout.WEST);
        pnlTmp.add(pnlLeft, BorderLayout.EAST);
        add(pnlTmp, BorderLayout.WEST);

        add(pnlCenter, BorderLayout.CENTER);

        pnlTmp = new JPanel(new BorderLayout());
        pnlTmp.add(pnlRight, BorderLayout.WEST);
        pnlTmp.add(pnlRightMost, BorderLayout.EAST);
        add(pnlTmp, BorderLayout.EAST);
    }

    public void addColumn(final StatusBarColumn column) {
        synchronized (columns) {
            if (!columns.contains(column)) {
                columns.add(column);
            }
        }

        Runnable worker = new Runnable() {
            public void run() {
                final Container container = getCorrespondingContainer(column);
                if (container == pnlLeftMost) {
                    if (pnlLeftMost.getComponentCount() > 0) {
                        final Component old = pnlLeftMost.getComponent(0);
                        pnlLeftMost.removeAll();
                        pnlLeft.add(old, 0);
                        pnlLeftMost.add(getWrapper(column));
                    } else {
                        pnlLeftMost.add(getWrapper(column));
                    }
                } else if (container == pnlRightMost) {
                    if (pnlRightMost.getComponentCount() > 0) {
                        final Component old = pnlRightMost.getComponent(0);
                        pnlRightMost.removeAll();
                        pnlRight.add(old, 0);
                        pnlRightMost.add(getWrapper(column));
                    } else {
                        pnlRightMost.add(getWrapper(column));
                    }
                } else {
                    container.add(getWrapper(column));
                }
            }
        };

        UIUtil.executeOnSwing(worker);
    }

    public void removeColumn(final StatusBarColumn column) {
        synchronized (columns) {
            columns.remove(column);
        }

        // to-do: replace everywhere with SwingRunnable in order to easily find all instances
        // to-do: also create a new interface SwingDelegate used for querying Swing objects and returning values
        Runnable worker = new Runnable() {
            public void run() {
                final Container container = getCorrespondingContainer(column);
                container.remove(getWrapper(column));
            }
        };

        UIUtil.executeOnSwing(worker);
    }

    public void setMessage(final String text) {
        colTasks.setText(text);
    }

    public BackgroundActionsColumn getBackgroundColumn() {
        return colTasks;
    }

    public void dispose() {}

    protected Container getCorrespondingContainer(final StatusBarColumn column) {
        switch (column.getPlacement()) {
            case StatusBarColumn.PLACEMENT_LEFTMOST:
                return pnlLeftMost;
            case StatusBarColumn.PLACEMENT_LEFT:
                return pnlLeft;
            case StatusBarColumn.PLACEMENT_CENTER:
                return pnlCenter;
            case StatusBarColumn.PLACEMENT_RIGHT:
                return pnlRight;
            case StatusBarColumn.PLACEMENT_RIGHTMOST:
                return pnlRightMost;
            default:
                return pnlLeft;
        }
    }

    protected JComponent getWrapper(final StatusBarColumn column) {
        JComponent wrapper;
        synchronized (wrappers) {
            wrapper = wrappers.get(column);

            if (wrapper == null) {
                wrapper = createWrapper(column);
                wrappers.put(column, wrapper);
            }
        }

        return wrapper;
    }

    protected JComponent createWrapper(final StatusBarColumn column) {
        JComponent wrapper;

        if (column.getWidth() > 0) {
            wrapper = new FixedWidthWrapper(column.getWidth());
        } else {
            switch (column.getPlacement()) {
                case StatusBarColumn.PLACEMENT_LEFT:
                case StatusBarColumn.PLACEMENT_RIGHT:
                case StatusBarColumn.PLACEMENT_CENTER:
                    wrapper = new FlowWrapper();
                    break;
                default:
                    wrapper = new Wrapper();
                    break;
            }
        }

        wrapper.add(column.getRenderer(), BorderLayout.CENTER);

        switch (column.getBorderType()) {
            case StatusBarColumn.BORDER_NONE:
                wrapper.setBorder(BORDER_NONE);
                break;
            case StatusBarColumn.BORDER_RAISED:
                wrapper.setBorder(BORDER_RAISED);
                break;
            case StatusBarColumn.BORDER_SUNKEN:
                wrapper.setBorder(BORDER_SUNKEN);
                break;
            case StatusBarColumn.BORDER_SIMPLE:
                wrapper.setBorder(BORDER_SIMPLE);
                break;
        }

        switch (column.getSeparator()) {
            case StatusBarColumn.SEPARATOR_NONE:
                break;
            case StatusBarColumn.SEPARATOR_LEFT:
                wrapper.add(UIUtil.createToolbarSeparator(), BorderLayout.WEST);
                break;
            case StatusBarColumn.SEPARATOR_RIGHT:
                wrapper.add(UIUtil.createToolbarSeparator(), BorderLayout.EAST);
                break;
            case StatusBarColumn.SEPARATOR_BOTH:
                wrapper.add(UIUtil.createToolbarSeparator(), BorderLayout.WEST);
                wrapper.add(UIUtil.createToolbarSeparator(), BorderLayout.EAST);
                break;
        }

        return wrapper;
    }

    // to-do: move outside of class
    private static class Wrapper extends JPanel {
        private Wrapper() {
            super(new BorderLayout());
        }
    }

    // to-do: move outside of class
    private static class FlowWrapper extends JPanel {
        private FlowWrapper() {
            super(new BorderLayout());
        }

        public Dimension getMinimumSize() {
            return new Dimension( (int) super.getMinimumSize().getWidth(), FIXED_HEIGHT - 8);
        }

        public Dimension getPreferredSize() {
            return new Dimension( (int) super.getPreferredSize().getWidth(), FIXED_HEIGHT - 8);
        }

        public Dimension getMaximumSize() {
            return new Dimension( (int) super.getMaximumSize().getWidth(), FIXED_HEIGHT - 8);
        }
    }

    // to-do: move outside of class
    private static class FixedWidthWrapper extends JPanel {
        private final int width;

        private FixedWidthWrapper(final int width) {
            super(new BorderLayout());
            this.width = width;
        }

        public Dimension getMinimumSize() {
            return new Dimension(width, (int) super.getMinimumSize().getHeight());
        }

        public Dimension getPreferredSize() {
            return new Dimension(width, (int) super.getPreferredSize().getHeight());
        }

        public Dimension getMaximumSize() {
            return new Dimension(width, (int) super.getMaximumSize().getHeight());
        }
    }
}