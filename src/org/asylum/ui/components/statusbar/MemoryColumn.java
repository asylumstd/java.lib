/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.statusbar;

import org.asylum.ui.UIUtil;
import static org.asylum.services.ServiceRegistry.getService;
import org.asylum.services.TaskService;
import org.asylum.ui.components.toolbar.ToolBarButton;
import org.asylum.lang.Disposable;
import org.asylum.monitor.GenericObserver;
import org.asylum.monitor.MemoryCollector;
import org.asylum.monitor.MemoryObserver;
import org.asylum.monitor.Watchdog;
import org.asylum.SimpleTask;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.MessageFormat;

/**
 * Author: Adrian Libotean
 * Date: Aug 4, 2009
 * Time: 10:13:34 AM
 */
public class MemoryColumn extends GenericColumn implements Disposable {
    private static final long ONE_MEGABYTE = 1048576;
    //--
    protected ToolBarButton btnMemory;
    protected JProgressBar pbMemory;
    protected GenericObserver observer;
    protected Long freePercent;
    protected Long freeMegabytes;

    public MemoryColumn(final MultiColumnStatusBar statusBar) {
        super(statusBar, PLACEMENT_RIGHTMOST, BORDER_NONE, SEPARATOR_LEFT);

        this.observer = null;
        freePercent = freeMegabytes = Long.valueOf(0);
    }

    protected void initComponents() {
        btnMemory = new ToolBarButton();
        btnMemory.setBorder(BorderFactory.createEmptyBorder(1, 2, 1, 2));
        btnMemory.setIcon(UIUtil.getIcon(this, "~/icons/gc.png"));
        btnMemory.setToolTipText(UIUtil.getString(this, "button.tooltip.text"));
        btnMemory.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                final TaskService service = getService(TaskService.class);
                service.execute(new SimpleTask<Void>(UIUtil.getString(MemoryColumn.this, "gc.text")) {
                    protected Void doInBackground() {
                        System.gc();
                        Runtime.getRuntime().gc();

                        return null;
                    }
                });
            }
        });

        pbMemory = new JProgressBar() {
            //-- lazy generation of tooltip
            public String getToolTipText() {
                MessageFormat fmt = UIUtil.getMessageFormatter(UIUtil.getString(MemoryColumn.this, "tooltip.text"));

                final Object[] arguments = new Object[] {
                    freePercent,
                    freeMegabytes
                };
                
                return fmt.format(arguments);
            }
        };

        ToolTipManager toolTipManager = ToolTipManager.sharedInstance();
        toolTipManager.registerComponent(pbMemory);
        
        pbMemory.setPreferredSize(new Dimension(60, 16));
        pbMemory.setMinimum(0);
        pbMemory.setMaximum(100);
        pbMemory.setValue(10);

        observer = new MemoryObserver(pbMemory, btnMemory) {
            protected void refresh(MemoryCollector.MemoryBundle bundle) {
                super.refresh(bundle);

                freePercent = Long.valueOf(bundle.getFreePercent());
                freeMegabytes = Long.valueOf(bundle.getFree() / ONE_MEGABYTE);
            }
        };

        final Watchdog watchdog = getService(Watchdog.class);
        watchdog.addObserver(observer);

        renderer.add(btnMemory, BorderLayout.WEST);
        renderer.add(pbMemory, BorderLayout.EAST);
    }

    public void dispose() {
        ToolTipManager toolTipManager = ToolTipManager.sharedInstance();
        toolTipManager.unregisterComponent(pbMemory);

        final Watchdog watchdog = getService(Watchdog.class);
        watchdog.removeObserver(observer);
    }
}