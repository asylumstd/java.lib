/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.ui.components.statusbar;

import org.asylum.ui.UIUtil;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;

public class SimpleStatusBar extends JPanel {
    protected static final int MINIMUM_HEIGHT = 21;
    //--
    protected JLabel lblStatus;

    public SimpleStatusBar() {
        super(new BorderLayout());

        initLayout();
        initComponents();
    }

    protected void initLayout() {
        setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));

        setMinimumSize(new Dimension( (int) getMinimumSize().getWidth(), MINIMUM_HEIGHT ));
        setPreferredSize(getMinimumSize());
    }

    protected void initComponents() {
        lblStatus = new JLabel();
        lblStatus.setBorder(BorderFactory.createEmptyBorder(1, 4, 1, 4));

        add(lblStatus, BorderLayout.CENTER);
    }

    public void setMessage(final String text) {
        Runnable worker = new Runnable() {
            public void run() {
                lblStatus.setText(text);
                lblStatus.invalidate();
                lblStatus.repaint();
            }
        };

        UIUtil.executeOnSwing(worker);
    }
}