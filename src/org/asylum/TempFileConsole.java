/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

/**
 * Author: Adrian Libotean
 * Date: Aug 10, 2009
 * Time: 2:36:17 PM
 * Summary: logs console (out, err) output to a local file
 */
public class TempFileConsole extends BufferedConsole {
    private File fileOut;
    private File fileErr;

    public TempFileConsole() {
        super(4096);
    }

    public void startUp() {
        try {
            fileOut = File.createTempFile("out", null);
            fileErr = File.createTempFile("err", null);
        } catch (IOException ex) {
            throw new IllegalStateException("Could not create temporary files", ex);
        }

        super.startUp();
    }

    public String getOutPath() {
        return fileOut.getAbsolutePath();
    }

    public String getErrPath() {
        return fileErr.getAbsolutePath();
    }

    protected FileOutputBuffer createOutBuffer(int size) {
        try {
            return new FileOutputBuffer(new FileOutputStream(fileOut), size);
        } catch (FileNotFoundException ignore) {}

        return null;
    }

    protected FileOutputBuffer createErrBuffer(int size) {
        try {
            return new FileOutputBuffer(new FileOutputStream(fileErr), size);
        } catch (FileNotFoundException ignore) {}

        return null;
    }

    private static class FileOutputBuffer implements OutputBuffer {
        private final FileChannel channel;
        private final ByteBuffer buffer;

        private FileOutputBuffer(final FileOutputStream stream, final int size) {
            this.channel = stream.getChannel();
            this.buffer = ByteBuffer.allocate(size);
        }

        public int capacity() {
            return Integer.MAX_VALUE;
        }

        public int remaining() {
            return Integer.MAX_VALUE;
        }

        public void rewind() {
            buffer.rewind();
        }

        public void position(int position) {
            buffer.position(position);
        }

        public void mark() {
            buffer.mark();
        }

        public void compact() {
            buffer.compact();
        }

        public void put(String text) {
            if (text != null) {
                put(text, 0, text.length());
            }
        }

        public void put(String text, int index, int length) {
            final byte[] bytes = new byte[length];
            final char[] chars = text.toCharArray();

            // to-do: optimize conversion
            for (int idx = index; idx < length; idx++) {
                bytes[idx] = (byte) chars[idx];
            }

            buffer.put(bytes);
            writeBuffer();
        }

        public void flush() {
            writeBuffer();

            try {
                channel.close();
            } catch (IOException ignore) {}
        }

        private void writeBuffer() {
            try {
                final FileLock lock = channel.tryLock();
                try {
                    buffer.flip();
                    channel.write(buffer);
                    buffer.clear();
                } finally {
                    lock.release();
                }
            } catch (IOException ignore) {}
        }
    }
}