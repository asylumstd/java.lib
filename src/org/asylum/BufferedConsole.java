/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum;

import java.nio.CharBuffer;

/**
 * Author: adrian.libotean
 * Date: Dec 9, 2009
 * Time: 2:58:11 PM
 */
public class BufferedConsole implements Console {
    final int bufferSize;
    OutputBuffer bufferOut;
    OutputBuffer bufferErr;

    public BufferedConsole(final int size) {
        super();

        this.bufferSize = size;

        this.bufferOut = null;
        this.bufferErr = null;
    }

    public void startUp() {
        this.bufferOut = createOutBuffer(bufferSize);
        this.bufferErr = createErrBuffer(bufferSize);
    }

    public void shutdown() {
        this.bufferOut = null;
        this.bufferErr = null;
    }

    public Console print(CharSequence sequence) {
        put(bufferOut, sequence.toString());

        return this;
    }

    public Console println(CharSequence sequence) {
        put(bufferOut, sequence + "\n");
        return this;
    }

    public void flush() {
        bufferOut.flush();
        bufferErr.flush();
    }

    public void error(String message) {
        put(bufferErr, "Error: ");
        put(bufferErr, message);
        put(bufferErr, "\n");
    }

    public void error(String message, Throwable ex) {
        put(bufferErr, "Error: ");
        put(bufferErr, message);
        put(bufferErr, ex.getStackTrace().toString());
        put(bufferErr, "\n");
    }

    public void warning(String message) {
        put(bufferOut, "Warning: ");
        put(bufferOut, message);
        put(bufferOut, "\n");
    }

    public void info(String message) {
        put(bufferOut, "Info: ");
        put(bufferOut, message);
        put(bufferOut, "\n");
    }

    private void put(final OutputBuffer buffer, final String text) {
        if (text.length() > buffer.capacity()) {
            buffer.rewind();
            buffer.put(text, 0, buffer.remaining());
        } else if (text.length() <= buffer.remaining()) {
            buffer.put(text, 0, text.length());
        } else {
            buffer.position(text.length() - buffer.remaining());
            buffer.mark();
            buffer.compact();

            buffer.put(text);
        }
    }

    protected OutputBuffer createOutBuffer(final int size) {
        return new CharOutputBuffer(size);
    }

    protected OutputBuffer createErrBuffer(final int size) {
        return new CharOutputBuffer(size);
    }

    private static class CharOutputBuffer implements OutputBuffer {
        private final CharBuffer buffer;

        private CharOutputBuffer(final int size) {
            buffer = CharBuffer.allocate(size);
        }

        public int capacity() {
            return buffer.capacity();
        }

        public int remaining() {
            return buffer.remaining();
        }

        public void rewind() {
            buffer.rewind();
        }

        public void position(int position) {
            buffer.position(position);
        }

        public void mark() {
            buffer.mark();
        }

        public void compact() {
            buffer.compact();
        }

        public void put(String text) {
            buffer.put(text);
        }

        public void put(String text, int index, int length) {
            buffer.put(text, index, length);
        }

        public void flush() {
            rewind();
        }
    }
}