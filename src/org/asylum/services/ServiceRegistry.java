/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.services;

import java.util.Map;
import java.util.HashMap;
import java.util.Stack;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.lang.reflect.Method;

/**
 * Author: adrian.libotean
 * Date: Nov 25, 2009
 * Time: 4:36:25 PM
 * Summary: A simple implementation of a thread safe service locator (registry)
 */
public class ServiceRegistry {
    private static final ServiceRegistry instance = new ServiceRegistry();
    private final Map<Class, Object> services;

    ServiceRegistry() {
        super();

        this.services = new HashMap<Class, Object>();
    }

    private Map<Class, Object> getServices() {
        return services;
    }

    public static <T> void register(final Class klass, final T service) {
        synchronized (instance) {
            final Object existing = instance.getServices().get(klass);
            if (existing instanceof Stack) {
                final Stack stack = (Stack) existing;
                stack.add(service);
            } else if (existing != null) {
                final Stack stack = new Stack();
                stack.add(existing);
                stack.add(service);

                instance.getServices().put(klass, stack);
            } else {
                instance.getServices().put(klass, service);
            }
        }
    }

    public static void unregister(final Class klass) {
        synchronized (instance) {
            final Object existing = instance.getServices().get(klass);
            if (existing instanceof Stack) {
                final Stack stack = (Stack) existing;
                stack.pop();
                
                if (stack.size() == 1) {
                    Object service = stack.pop();
                    instance.getServices().put(klass, service);
                }
            } else if (existing != null) {
                instance.getServices().remove(klass);
            }
        }
    }

    public static <T> T getService(final Class<T> klass) {
        final Object candidate = __getService(klass);
        return (T) __wrapper(klass, candidate);
    }

    private static <T> T __getService(final Class<T> klass) {
        Object candidate;
        synchronized (instance) {
            candidate = instance.getServices().get(klass);
        }

        if (candidate == null) {
            throw new ServiceHandlerNotRegisteredError(String.format("A service handler for %s was not registered", klass));
        } else if (candidate instanceof Stack) {
            candidate = ((Stack) candidate).peek();
        }

        return (T) candidate;
    }

    private static <T> Object __wrapper(final Class klass, final T service) {
        Object proxy = null;
        try {
            proxy = Proxy.newProxyInstance(service.getClass().getClassLoader(), new Class[] { klass },
                new ServiceWrapper(klass));
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }

        return proxy;
    }

    private static class ServiceWrapper<T> implements InvocationHandler {
        private final Class klass;

        private ServiceWrapper(final Class klass) {
            this.klass = klass;
        }

        public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
            final T service = (T) __getService(klass);
            final Object[] arguments = (args != null) ? args : new Object[] {};

            final Method[] m = service.getClass().getMethods();
            if (m != null && m.length > 0) {
                for (final Method candidate : m) {
                    if (method.getName().equals(candidate.getName())) {
                        final Class[] paramTypes = (candidate.getParameterTypes() != null) ? candidate.getParameterTypes() : new Class[] {};
                        if (paramTypes.length == 0 && arguments.length == 0) {
                            final boolean prevState = candidate.isAccessible();
                            candidate.setAccessible(true);
                            try {
                                return candidate.invoke(service, args);
                            } finally {
                                candidate.setAccessible(prevState);
                            }
                        } else if (paramTypes.length == arguments.length) {
                            boolean incompatible = false;
                            for (int idx = 0; idx < paramTypes.length; idx++) {
                                if (!typesMatch(arguments[idx], paramTypes[idx])) {
                                    incompatible = true;
                                    break;
                                }
                            }

                            if (!incompatible) {
                                final boolean prevState = candidate.isAccessible();
                                candidate.setAccessible(true);
                                try {
                                    return candidate.invoke(service, args);
                                } finally {
                                    candidate.setAccessible(prevState);
                                }
                            }
                        }
                    }
                }
            }

            throw new IllegalArgumentException("Invalid method call");
        }

        private static boolean typesMatch(final Object value, final Class klass) {
            if (value == null) {
                return true;
            }

            if (klass.isPrimitive()) {
                if (klass == Integer.TYPE) {
                    return value instanceof Integer;
                } else if (klass == Boolean.TYPE) {
                    return value instanceof Boolean;
                } else if (klass == Character.TYPE) {
                    return value instanceof Character;
                } else if (klass == Byte.TYPE) {
                    return value instanceof Byte;
                } else if (klass == Short.TYPE) {
                    return value instanceof Short;
                } else if (klass == Long.TYPE) {
                    return value instanceof Long;
                } else if (klass == Float.TYPE) {
                    return value instanceof Float;
                } else if (klass == Double.TYPE) {
                    return value instanceof Double;
                } else if (klass == Void.TYPE) {
                    return value instanceof Void;
                }
            } else {
                return klass.isAssignableFrom(value.getClass());
            }

            return false;
        }
    }
}