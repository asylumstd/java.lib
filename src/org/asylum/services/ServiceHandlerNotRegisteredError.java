/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.services;

/**
 * Author: adrian.libotean
 * Date: Nov 25, 2009
 * Time: 4:52:53 PM
 */
public class ServiceHandlerNotRegisteredError extends RuntimeException {
    public ServiceHandlerNotRegisteredError(String s) {
        super(s);
    }

    public ServiceHandlerNotRegisteredError(String s, Throwable throwable) {
        super(s, throwable);
    }
}