/*
 * Copyright (c) 2011 Asylum Studios Romania.
 */

package org.asylum.services;

import org.asylum.Task;

/**
 * Author: adrian.libotean
 * Date: 08/04/2011
 * Time: 12:35:06 PM
 * Summary:
 */
public interface TaskService extends Service {
    void execute(Task task);
}
