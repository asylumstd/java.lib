/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.services;

/**
 * Author: adrian.libotean
 * Date: Nov 25, 2009
 * Time: 4:36:40 PM
 */
public interface Service {
    String getName();
}