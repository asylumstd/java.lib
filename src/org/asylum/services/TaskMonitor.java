/*
 * Copyright (c) 2011 Asylum Studios Romania.
 */

package org.asylum.services;

import org.asylum.Task;

import java.beans.PropertyChangeListener;

/**
 * Author: adrian.libotean
 * Date: 08/04/2011
 * Time: 12:35:12 PM
 * Summary:
 */
public interface TaskMonitor extends Service {
    public static final String PROPERTY_TASKS = "tasks";
    public static final String PROPERTY_FOREGROUND_TASK = "foreground-task";

    Iterable<Task> getTasks();

    void addPropertyChangeListener(PropertyChangeListener listener);
    void removePropertyChangeListener(PropertyChangeListener listener);
}