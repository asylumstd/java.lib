/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.cache;

/**
 * Author: adrian.libotean
 * Date: Nov 27, 2009
 * Time: 3:47:03 PM
 */
public interface Cache<K, V> {
    V store(K key, V value);
    boolean isCached(K key);
    V load(K key);
}