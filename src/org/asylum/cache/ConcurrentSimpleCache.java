/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Author: adrian.libotean
 * Date: Nov 27, 2009
 * Time: 4:02:52 PM
 */
public class ConcurrentSimpleCache<K, V> extends SimpleCache<K, V> {
    protected Map<K, V> createCache() {
        return new ConcurrentHashMap<K, V>();
    }
}