/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.cache;

import org.asylum.cache.policies.CachePolicy;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Author: adrian.libotean
 * Date: Nov 27, 2009
 * Time: 3:47:11 PM
 */
public abstract class AdaptiveCache<K, V> implements SmartCache<K, V> {
    private AtomicLong hits;
    private AtomicLong misses;
    private AtomicLong queries;
    private final Map<K, ItemWrapper<V>> cache;
    private final CachePolicy<K, V> policy;

    protected AdaptiveCache(final CachePolicy<K, V> policy) {
        super();

        this.cache = createCache();
        this.policy = policy;
        this.hits = new AtomicLong(0L);
        this.misses = new AtomicLong(0L);
        this.queries = new AtomicLong(0L);
    }

    protected Map<K, ItemWrapper<V>> createCache() {
        return new HashMap<K, ItemWrapper<V>>();
    }

    public long getHits() {
        return hits.get();
    }

    public long getMisses() {
        return misses.get();
    }

    public long getQueries() {
        return queries.get();
    }

    public V store(final K key, final V value) {
        final ItemWrapper<V> wrapper = policy.getWrapper(key, value);
        cache.put(key, wrapper);
        return wrapper.get();
    }

    public boolean isCached(final K key) {
        return cache.containsKey(key);
    }

    public V load(final K key) {
        final ItemWrapper<V> wrapper = cache.get(key);
        return (wrapper == null) ? null : wrapper.get();
    }

    abstract public V generate(K key);
}