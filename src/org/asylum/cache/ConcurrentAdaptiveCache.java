/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.cache;

import org.asylum.cache.policies.CachePolicy;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Author: adrian.libotean
 * Date: Nov 27, 2009
 * Time: 3:47:27 PM
 */
public abstract class ConcurrentAdaptiveCache<K, V> extends AdaptiveCache<K, V> {

    protected ConcurrentAdaptiveCache(final CachePolicy<K, V> policy) {
        super(policy);
    }

    protected Map<K, ItemWrapper<V>> createCache() {
        return new ConcurrentHashMap<K, ItemWrapper<V>>();
    }
}