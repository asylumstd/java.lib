/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.cache;

/**
 * Author: adrian.libotean
 * Date: Nov 27, 2009
 * Time: 3:52:26 PM
 */
public interface WeakCache<K, V> extends Cache<K, V> {
    V generate(K key);
}