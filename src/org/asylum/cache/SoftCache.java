/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.cache;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

/**
 * Author: adrian.libotean
 * Date: Nov 27, 2009
 * Time: 3:46:56 PM
 * Summary: Simple implementation of a cache that holds soft references to cached values
 */
public abstract class SoftCache<K, V> implements WeakCache<K, V> {
    private final Map<K, SoftReference<V>> cache;

    protected SoftCache() {
        super();

        this.cache = createCache();
    }

    protected Map<K, SoftReference<V>> createCache() {
        return new HashMap<K, SoftReference<V>>();
    }

    public V store(final K key, final V value) {
        SoftReference<V> reference = new SoftReference<V>(value);
        cache.put(key, reference);
        return value;
    }

    public boolean isCached(final K key) {
        return cache.containsKey(key);
    }

    public V load(final K key) {
        final SoftReference<V> reference = cache.get(key);

        if (reference == null) {
            return null;
        } else {
            V result = reference.get();

            if (result == null) {
                result = generate(key);
                cache.put(key, new SoftReference<V>(result));
            }

            return result;            
        }
    }

    public abstract V generate(K key);
}