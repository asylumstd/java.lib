/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.cache.policies;

import org.asylum.cache.AdaptiveCache;
import org.asylum.cache.ItemWrapper;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: adrian.libotean
 * Date: Nov 27, 2009
 * Time: 5:06:13 PM
 * Summary: Will maintain N top hit entries as strong references whilst the others are kept as soft references
 */
public class TopHitsCachePolicy<K, V> implements CachePolicy<K, V> {
    private AdaptiveCache<K, V> cache;
    private final int strongRefsCount;
    private final List<TopHitsWrapper> topHits;

    public TopHitsCachePolicy(int strongReferencesSize) {
        super();

        this.strongRefsCount = strongReferencesSize;
        this.topHits = new ArrayList<TopHitsWrapper>();
    }

    public ItemWrapper<V> getWrapper(K key, V value) {
        return new TopHitsWrapper(key, value);
    }

    public synchronized void setCache(AdaptiveCache<K, V> cache) {
        this.cache = cache;
    }

    class TopHitsWrapper implements ItemWrapper<V> {
        private long hitCount;
        private final K key;
        private Reference<V> reference;
        private V value;

        private TopHitsWrapper(final K key, final V value) {
            this.key = key;
            this.value = null;
            this.reference = new SoftReference<V>(value);
            this.hitCount = 0;
        }

        public synchronized long getHitCount() {
            return hitCount;
        }

        public synchronized V get() {
            hitCount += 1;

            if (value != null) {
                return value;
            } else {
                return getValue();
            }
        }

        private synchronized V getValue() {
            V result = reference.get();
            if (result == null) {
                result = cache.generate(key);
                reference = new SoftReference<V>(result);
            }

            checkRanking();
            return result;
        }

        private void checkRanking() {
            synchronized (topHits) {
                //-- not enabled
                if (strongRefsCount <= 0) {
                    return;
                }

                //-- check if still avaialable entries
                if (topHits.size() < strongRefsCount) {
                    this.promote();
                    topHits.add(this);
                } else {
                    for (final TopHitsWrapper candidate : topHits) {
                        if (candidate.getHitCount() < hitCount) {
                            candidate.demote();
                            topHits.remove(candidate);
                            topHits.add(this);

                            break;
                        }
                    }
                }
            }
        }

        private void promote() {
            value = reference.get();
            reference = null;

            if (value == null) {
                value = cache.generate(key);
            }
        }

        private void demote() {
            reference = new SoftReference<V>(value);
            value = null;
        }
    }
}