/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.cache.policies;

import org.asylum.cache.ItemWrapper;
import org.asylum.cache.AdaptiveCache;

/**
 * Author: adrian.libotean
 * Date: Nov 27, 2009
 * Time: 4:59:38 PM
 */
public interface CachePolicy<K, V> {
    ItemWrapper<V> getWrapper(K key, V value);
    void setCache(AdaptiveCache<K, V> cache);
}