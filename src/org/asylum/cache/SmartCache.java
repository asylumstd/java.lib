/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.cache;

/**
 * Author: adrian.libotean
 * Date: Nov 27, 2009
 * Time: 3:48:02 PM
 */
public interface SmartCache<K, V> extends WeakCache<K, V> {
    long getHits();
    long getMisses();
    long getQueries();
}