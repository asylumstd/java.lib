/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.cache;

import java.lang.ref.SoftReference;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Author: adrian.libotean
 * Date: Nov 27, 2009
 * Time: 3:47:18 PM
 */
public abstract class ConcurrentSoftCache<K, V> extends SoftCache<K, V> {

    protected Map<K, SoftReference<V>> createCache() {
        return new ConcurrentHashMap<K, SoftReference<V>>();
    }
}