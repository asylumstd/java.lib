/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.cache;

/**
 * Author: adrian.libotean
 * Date: Nov 27, 2009
 * Time: 5:03:17 PM
 */
public interface ItemWrapper<T> {
    T get();
}