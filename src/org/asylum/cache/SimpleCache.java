/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.cache;

import java.util.HashMap;
import java.util.Map;

/**
 * Author: adrian.libotean
 * Date: Nov 27, 2009
 * Time: 4:00:35 PM
 * Summary: A very simple cache implementation
 */
public class SimpleCache<K, V> implements Cache<K, V> {
    private final Map<K, V> cache;

    public SimpleCache() {
        super();

        this.cache = createCache();
    }

    protected Map<K, V> createCache() {
        return new HashMap<K, V>();
    }

    public V store(final K key, final V value) {
        cache.put(key, value);
        return value;
    }

    public boolean isCached(final K key) {
        return cache.containsKey(key);
    }

    public V load(final K key) {
        return cache.get(key);
    }
}