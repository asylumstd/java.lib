/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum;

import java.util.Arrays;

/**
 * Author: adrian.libotean
 * Date: Nov 26, 2009
 * Time: 10:17:13 AM
 */
public class ArraysUtil {
    private ArraysUtil() {}

    public static <T> T[] copy(T[] source) {
        return (source == null) ? null : Arrays.copyOf(source, source.length);
    }

    public static byte[] copy(byte[] source) {
        return (source == null) ? null : Arrays.copyOf(source, source.length);
    }

    public static int[] copy(int[] source) {
        return (source == null) ? null : Arrays.copyOf(source, source.length);
    }

    public static long[] copy(long[] source) {
        return (source == null) ? null : Arrays.copyOf(source, source.length);
    }
}