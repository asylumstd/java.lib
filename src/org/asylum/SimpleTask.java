/*
 * Copyright (c) 2011 Asylum Studios Romania.
 */

package org.asylum;

import javax.swing.*;
import static javax.swing.SwingWorker.StateValue.PENDING;
import static javax.swing.SwingWorker.StateValue.STARTED;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

/**
 * Author: adrian.libotean
 * Date: 08/04/2011
 * Time: 12:49:43 PM
 * Summary:
 */
public abstract class SimpleTask<T> extends SwingWorker<Void, T> implements Task {
    private static final String PROP_STATE = "state";
    //--
    private final String title;

    protected SimpleTask(final String title) {
        super();

        this.title = title;
        addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(final PropertyChangeEvent evt) {
                if (PROP_STATE.equals(evt.getPropertyName())) {
                    //-- ignore non-final state
                    if (PENDING == getState() || STARTED == getState()) {
                        return;
                    }

                    if (isCancelled()) {
                        onCanceled();
                    } else if (isDone()) {
                        onFinished();
                    }
                }
            }
        });
    }

    public String getTitle() {
        return title;
    }

    public void onCanceled() {}
    public void onFinished() {}
}