/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.configuration;

/**
 * Author: adrian.libotean
 * Date: Nov 26, 2009
 * Time: 3:54:04 PM
 */
public class PathElementImpl implements PathElement {
    private final boolean section;
    private final String name;

    public PathElementImpl(final String name, final boolean section) {
        this.name = name;
        this.section = section;
    }

    public String getName() {
        return name;
    }

    public boolean isSection() {
        return section;
    }
}