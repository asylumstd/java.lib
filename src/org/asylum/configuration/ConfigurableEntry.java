/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.configuration;

/**
 * Author: adrian.libotean
 * Date: Dec 2, 2009
 * Time: 4:11:24 PM
 */
public interface ConfigurableEntry<T> extends Entry<T> {
    String getClassName();
    String getMemberName();
}