/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.configuration;

import java.awt.*;
import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Author: adrian.libotean
 * Date: Nov 26, 2009
 * Time: 4:51:43 PM
 */
public class INIConfigurationReader implements ConfigurationReader {
    private static final String PATTERN_SECTION = "^\\[([a-zA-Z\\.]+)\\]$";
    private static final String PATTERN_CONFIGURABLE_SECTION = "^\\[@([a-zA-Z\\.]+)\\]$";
    private static final String PATTERN_COMMENTS = "^\\s*#.*$";
    private static final String PATTERN_ENTRY = "^([a-zA-Z_\\.\\*]+)=(.+)$";
    private static final String PATTERN_VALUE = "^([a-zA-Z\\.]+),(.+)$";
    //--
    private static final Pattern rxSection = Pattern.compile(PATTERN_SECTION);
    private static final Pattern rxConfigurableSection = Pattern.compile(PATTERN_CONFIGURABLE_SECTION);
    private static final Pattern rxComments = Pattern.compile(PATTERN_COMMENTS);
    private static final Pattern rxEntry = Pattern.compile(PATTERN_ENTRY);
    private static final Pattern rxValue = Pattern.compile(PATTERN_VALUE);
    //--
    private final Collection<String> filenames;
    private Section root;
    private Section section;
    private String configurable_class;

    public INIConfigurationReader(String... filenames) {
        super();

        this.filenames = new ArrayList<String>();
        this.filenames.addAll(Arrays.asList(filenames));
        this.section = null;
        this.configurable_class = null;
    }

    public void readInto(final Section root) {
        for (final String filename : filenames) {
            final File file = new File(filename);
            if (file.exists() && file.isFile() && file.canRead()) {
                this.root = root;
                this.section = root;

                try {
                    final BufferedReader reader = new BufferedReader(new FileReader(file));
                    String line = reader.readLine();
                    while (line != null) {
                        parseLine(line);
                        line = reader.readLine();
                    }
                } catch (FileNotFoundException ex) {
                    throw new RuntimeException(String.format("Could not read INI file %s", filename), ex);
                } catch (IOException ex) {
                    throw new RuntimeException(String.format("Could not read INI file %s", filename), ex);
                }
            }
        }
    }

    private void parseLine(final String line) {
        if (line == null || line.length() == 0) {
            //-- skip
            return;
        }

        Matcher matcher = rxSection.matcher(line);
        if (matcher.matches()) {
            section = generateSection(matcher.group(1));
        } else {
            matcher = rxConfigurableSection.matcher(line);
            if (matcher.matches()) {
                section = generateConfigurableSection(matcher.group(1));
            } else {
                matcher = rxComments.matcher(line);
                if (matcher.matches()) {
                    //-- ignore comments
                } else {
                    matcher = rxEntry.matcher(line);
                    if (matcher.matches() && matcher.groupCount() == 2) {
                        generateEntry(matcher.group(1), matcher.group(2));
                    } else {
                        throw new IllegalINIFormatError(String.format("Could not parse line: %s", line));
                    }
                }
            }
        }
    }

    private Section generateSection(final String name) {
        configurable_class = null;
        section = root;

        final StringTokenizer tokenizer = new StringTokenizer(name, "./\\");
        if (tokenizer.countTokens() > 1) {
            while (tokenizer.hasMoreTokens()) {
                final String token = tokenizer.nextToken();

                Entry item = section.getEntry(token);
                if (item == null) {
                    item = new SectionImpl(token);
                    section.add(item);
                }

                section = (Section) item;
            }

            return section;
        } else {
            Entry<Section> item = new SectionImpl(name);
            section.add(item);
            return (Section) item;
        }
    }

    private Section generateConfigurableSection(final String klass) {
        configurable_class = klass;

        section = root;
        return section;
    }

    private Entry<Object> generateEntry(final String name, final String valueRepr) {
        final Object value = factoryValue(valueRepr);

        Entry<Object> entry;
        if (configurable_class == null) {
            entry = new EntryImpl<Object>(name, value);
        } else {
            entry = new ConfigurableEntryImpl<Object>(configurable_class, name, value);
        }

        section.add(entry);
        return entry;
    }

    private Object factoryValue(final String representation) {
        //-- try to parse class,value format
        final Matcher matcher = rxValue.matcher(representation);
        if (matcher.matches()) {
            final String className = matcher.group(1);
            final String string = matcher.group(2);

            if ("Boolean".equalsIgnoreCase(className)) {
                return Boolean.valueOf(string);
            } else if ("Integer".equalsIgnoreCase(className)) {
                return Integer.valueOf(string);
            } else if ("Long".equalsIgnoreCase(className)) {
                return Long.valueOf(string);
            } else if ("Character".equalsIgnoreCase(className) || "Char".equalsIgnoreCase(className)) {
                return Character.valueOf(string.toCharArray()[0]);
            } else if ("Float".equalsIgnoreCase(className)) {
                return Float.valueOf(string);
            } else if ("Double".equalsIgnoreCase(className)) {
                return Double.valueOf(string);
            } else if ("Color".equalsIgnoreCase(className)) {
                return Color.getColor(string);
            } else {
                try {
                    final Class klass = Class.forName(className);
                    final Constructor constructor = klass.getConstructor(String.class);
                    return constructor.newInstance(string);
                } catch (ClassNotFoundException ex) {
                    ex.printStackTrace();
                } catch (NoSuchMethodException ex) {
                    ex.printStackTrace();
                } catch (InvocationTargetException ex) {
                    ex.printStackTrace();
                } catch (IllegalAccessException ex) {
                    ex.printStackTrace();
                } catch (InstantiationException ex) {
                    ex.printStackTrace();
                }
            }
        }

        //-- try to parse actual value
        try {
            return Integer.valueOf(representation);
        } catch (NumberFormatException ignore) {
            try {
                return Float.valueOf(representation);
            } catch (NumberFormatException ignored) {
                return representation;
            }
        }
    }

    public static class IllegalINIFormatError extends RuntimeException {
        public IllegalINIFormatError(String s) {
            super(s);
        }

        public IllegalINIFormatError(String s, Throwable throwable) {
            super(s, throwable);
        }
    }
}