/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.configuration;

/**
 * Author: adrian.libotean
 * Date: Dec 2, 2009
 * Time: 4:11:53 PM
 */
public class ConfigurableEntryImpl<T> implements ConfigurableEntry<T> {
    private final String className;
    private final String memberName;
    private final T value;

    public ConfigurableEntryImpl(final String className, final String memberName, final T value) {
        this.className = className;
        this.memberName = memberName;
        this.value = value;
    }

    public String getClassName() {
        return className;
    }

    public String getMemberName() {
        return memberName;
    }

    public String getName() {
        return className + '.' + memberName;
    }

    public T getValue() {
        return value;
    }
}