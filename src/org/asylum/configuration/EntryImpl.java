/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.configuration;

/**
 * Author: adrian.libotean
 * Date: Nov 26, 2009
 * Time: 3:20:36 PM
 */
public class EntryImpl<T> implements Entry<T> {
    final String name;
    final T value;

    public EntryImpl(String name, T value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public T getValue() {
        return value;
    }
}