/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: adrian.libotean
 * Date: Nov 26, 2009
 * Time: 3:47:18 PM
 */
public class PathImpl implements Path {
    private static final Configurable<Integer> INITIAL_CAPACITY = new Configurable<Integer>(Integer.valueOf(8));
    private final List<PathElement> elements;
    private int cursor;

    PathImpl() {
        super();

        this.elements = new ArrayList<PathElement>(INITIAL_CAPACITY.get().intValue());
        this.cursor = -1;
    }

    public boolean isSection() {
        return false;
    }

    public String getName() {
        return null;
    }

    public boolean hasNext() {
        synchronized (elements) {
            return cursor < elements.size() - 1;
        }
    }

    public boolean isLeaf() {
        return !hasNext();
    }

    public PathElement next() {
        if (hasNext()) {
            synchronized (elements) {
                cursor += 1;
                return elements.get(cursor);
            }
        } else {
            throw new IllegalStateException("Path does not have any next element");
        }
    }

    public void reset() {
        this.cursor = 0;
    }

    void add(final PathElement element) {
        synchronized (elements) {
            elements.add(element);
        }
    }

    int size() {
        synchronized (elements) {
            return elements.size();
        }
    }
}