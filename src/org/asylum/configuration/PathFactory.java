/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.configuration;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Author: adrian.libotean
 * Date: Nov 26, 2009
 * Time: 3:57:29 PM
 */
public class PathFactory {
    private static final Configurable<Integer> INITIAL_CAPACITY = new Configurable<Integer>(Integer.valueOf(16));
    private static final Configurable<String> SEPARATORS = new Configurable<String>("./\\");
    //--
    private static final Map<String, SoftReference<Path>> cache =
        new HashMap<String, SoftReference<Path>>(INITIAL_CAPACITY.get().intValue());
    private static long hits = 0;
    private static long misses = 0;

    public static Path getPath(final String fullPath) {
        final SoftReference<Path> reference;

        synchronized (cache) {
            reference = cache.get(fullPath);
        }

        final Path existing = (reference != null) ? reference.get() : null;
        if (existing != null) {
            hits += 1;
            return existing;
        } else {
            misses += 1;

            final Path path = create(fullPath);
            synchronized (cache) {
                cache.put(fullPath, new SoftReference<Path>(path));
            }

            return path;
        }
    }

    public static long getHits() {
        return hits;
    }

    public static long getMisses() {
        return misses;
    }

    private static Path create(final String fullPath) {
        final PathImpl path = new PathImpl();
        final StringTokenizer tokenizer = new StringTokenizer(fullPath, SEPARATORS.get());

        while (tokenizer.hasMoreTokens()) {
            final String token = tokenizer.nextToken();

            if (tokenizer.hasMoreTokens()) {
                path.add(new PathElementImpl(token.trim(), true));
            } else {
                path.add(new PathElementImpl(token.trim(), false));
            }
        }

        return (path.size() > 0) ? path : null;
    }
}