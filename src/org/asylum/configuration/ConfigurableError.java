/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.configuration;

/**
 * Author: adrian.libotean
 * Date: Nov 26, 2009
 * Time: 2:07:40 PM
 */
public class ConfigurableError extends Exception {
    private final Configurable cfg;

    public ConfigurableError(String s, Configurable cfg) {
        super(s);
        this.cfg = cfg;
    }

    public ConfigurableError(String s, Throwable throwable, Configurable cfg) {
        super(s, throwable);
        this.cfg = cfg;
    }

    public Configurable getConfigurable() {
        return cfg;
    }
}