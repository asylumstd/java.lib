/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.configuration;

/**
 * Author: adrian.libotean
 * Date: Nov 26, 2009
 * Time: 3:12:32 PM
 */
public interface Configuration {
    Section getRoot();
    
    //-- searches root section for entry with specified name
    <T> Entry<T> getEntry(final String key);

    //-- searches full tree for entry and returns default value or actual value
    <T> T get(final Path path, final T defaultValue);
    //-- searches full tree for entry and returns default value or null
    <T> T get(final Path path);

    boolean contains(final Entry<Object> entry);
}