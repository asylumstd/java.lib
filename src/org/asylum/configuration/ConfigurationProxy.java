/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.configuration;

import java.lang.ref.SoftReference;

/**
 * Author: adrian.libotean
 * Date: Nov 27, 2009
 * Time: 12:52:33 PM
 * Summary: A proxy that maintains a soft reference to a configuration and re-generates it when necessary from the reader
 */
public class ConfigurationProxy implements Configuration {
    private SoftReference<Configuration> reference;
    private final ConfigurationReader reader;

    public ConfigurationProxy(ConfigurationReader reader) {
        this.reader = reader;
    }

    public Section getRoot() {
        final Configuration cfg = getConfiguration();
        return cfg.getRoot();
    }

    public <T> Entry<T> getEntry(final String key) {
        final Configuration cfg = getConfiguration();
        return cfg.getEntry(key);
    }

    public <T> T get(final Path path, final T defaultValue) {
        final Configuration cfg = getConfiguration();
        return cfg.get(path, defaultValue);
    }

    public <T> T get(final Path path) {
        final Configuration cfg = getConfiguration();
        return (T) cfg.get(path);
    }

    public boolean contains(final Entry<Object> entry) {
        final Configuration cfg = getConfiguration();
        return cfg.contains(entry);
    }

    private synchronized Configuration getConfiguration() {
        Configuration result = null;
        if (reference != null) {
            result = reference.get();
        }

        if (result == null) {
            result = new ConfigurationImpl();
            reader.readInto(result.getRoot());
            reference = new SoftReference<Configuration>(result);
        }

        return result;
    }
}