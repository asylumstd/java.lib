/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.configuration;

/**
 * Author: adrian.libotean
 * Date: Nov 26, 2009
 * Time: 3:46:15 PM
 */
public interface Path {
    boolean hasNext();
    boolean isLeaf();
    PathElement next();
    void reset();
}