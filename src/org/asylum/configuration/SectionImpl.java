/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.configuration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Author: adrian.libotean
 * Date: Nov 26, 2009
 * Time: 3:21:31 PM
 */
public class SectionImpl implements Section {
    private final String name;
    private final Collection<Entry<?>> entries;

    public SectionImpl(final String name) {
        super();

        this.name = name;
        this.entries = new ArrayList<Entry<?>>();
    }

    public Collection<Entry<?>> children() {
        synchronized (entries) {
            return Collections.unmodifiableCollection(entries);
        }
    }

    public void add(final Entry<?> entry) {
        synchronized (entries) {
            entries.add(entry);
        }
    }

    public void remove(final Entry<?> entry) {
        synchronized (entries) {
            entries.remove(entry);
        }
    }

    public boolean contains(final Entry<?> entry) {
        synchronized (entries) {
            return entries.remove(entry);
        }
    }

    public Entry<?> getEntry(final String key) {
        synchronized (entries) {
            return locate(entries, key, false);
        }
    }

    public Entry<?> findEntry(String key) {
        synchronized (entries) {
            return locate(entries, key, true);
        }
    }

    public String getName() {
        return name;
    }

    public Section getValue() {
        return this;
    }

    private Entry<?> locate(final Collection<Entry<?>> entries, final String key, final boolean recurse) {
        if (key != null && !"".equals(key.trim())) {
            //-- search for entry amongst all tree
            for (final Entry entry : entries) {
                if (recurse && entry instanceof Section) {
                    final Section section = (Section) entry;
                    return section.findEntry(key);
                } else if (key.equalsIgnoreCase(entry.getName())) {
                    return entry;
                }
            }
        }

        return null;
    }
}