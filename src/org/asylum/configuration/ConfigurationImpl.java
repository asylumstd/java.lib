/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.configuration;

/**
 * Author: adrian.libotean
 * Date: Nov 26, 2009
 * Time: 3:19:33 PM
 */
public class ConfigurationImpl implements Configuration {
    private final Section root;

    public ConfigurationImpl() {
        super();

        this.root = new SectionImpl("::root::");
    }

    public Section getRoot() {
        return root;
    }

    public <T> Entry<T> getEntry(final String key) {
        return (Entry<T>) root.getEntry(key);
    }

    public <T> T get(final Path path, T defaultValue) {
        Entry<T> entry;

        final PathElement first = path.next();
        if (!first.isSection()) {
            entry = getEntry(first.getName());
        } else {
            Section cursor = root;

            PathElement element = first;
            while (cursor != null && element.isSection()) {
                final Entry item = cursor.getEntry(element.getName());
                cursor = (Section) item;
                element = path.next();                
            }

            entry = (cursor != null) ? (Entry<T>) cursor.getEntry(element.getName()) : null;
        }

        return (entry != null) ? entry.getValue() : defaultValue;
    }

    public <T> T get(final Path path) {
        return get(path, (T) null);
    }

    public boolean contains(final Entry<Object> entry) {
        return root.contains(entry);
    }
}