/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.configuration;

/**
 * Author: adrian.libotean
 * Date: Nov 26, 2009
 * Time: 4:50:03 PM
 */
public interface ConfigurationReader {
    void readInto(final Section root);
}