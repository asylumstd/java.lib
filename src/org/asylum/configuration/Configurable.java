/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.configuration;

import java.lang.reflect.Field;
import java.util.Collection;

/**
 * Author: adrian.libotean
 * Date: Nov 26, 2009
 * Time: 1:48:46 PM
 */
public class Configurable<T> {
    private T value;
    private Policy policy;

    public Configurable(T value, Policy policy) {
        super();

        this.value = value;
        this.policy = policy;
    }

    public Configurable(T value) {
        this(value, Policy.WriteMany);
    }

    public T get() {
        return value;
    }

    private void set(T value) {
        this.value = value;
    }

    public Policy getPolicy() {
        return policy;
    }

    public String toString() {
        return String.valueOf(value);
    }

    public void notifyChange(T oldValue) {
        //-- do nothing
    }

    public static <T> void configure(final Configurable<T> cfg, T newValue) throws ConfigurableError {
        if (cfg.getPolicy() == Policy.ReadOnly) {
            throw new ConfigurableError(String.format("Configurable '%s' is read only", cfg), cfg);
        } else if (cfg.getPolicy() == Policy.WriteOnce && cfg.get() != null) {
            throw new ConfigurableError(String.format("Configurable '%s' already has a value and is write one", cfg), cfg);
        }

        final T previous = cfg.get();
        final boolean changed = (newValue == null) ? previous != null : !newValue.equals(previous);

        if (changed) {
            cfg.set(newValue);
            cfg.notifyChange(previous);
        }
    }

    public static <T> void tryConfigure(final Configurable<T> cfg, T newValue) {
        try {
            configure(cfg, newValue);
        } catch (ConfigurableError ignore) {}
    }

    public static void read(final Configuration cfg) {
        Collection<Entry<?>> children = cfg.getRoot().children();
        for (Entry<?> child : children) {
            if (child instanceof ConfigurableEntry) {
                final ConfigurableEntry cfgEntry = (ConfigurableEntry) child;
                read(cfgEntry.getClassName(), cfgEntry.getMemberName(), cfgEntry.getValue());
            }
        }
    }

    private static void read(final String className, final String memberName, final Object newValue) {
        try {
            final Class klass = Class.forName(className);
            final Field field = klass.getDeclaredField(memberName);
            final boolean accessible = field.isAccessible();
            field.setAccessible(true);
            try {
                final Configurable value = (Configurable) field.get(null);
                value.set(newValue);
            } finally {
                field.setAccessible(accessible);
            }
        } catch (ClassNotFoundException ex){
            ex.printStackTrace();
        } catch (NoSuchFieldException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        }
    }

    public static enum Policy {
        ReadOnly,
        WriteOnce,
        WriteMany,
    }
}