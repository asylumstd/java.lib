/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.configuration;

import java.util.Collection;

/**
 * Author: adrian.libotean
 * Date: Nov 26, 2009
 * Time: 3:12:43 PM
 */
public interface Section extends Entry<Section> {
    Collection<Entry<?>> children();

    void add(final Entry<?> entry);
    void remove(final Entry<?> entry);
    boolean contains(final Entry<?> entry);

    //-- searches for entries at section level
    Entry<?> getEntry(final String key);
    //-- searches full tree for entry
    Entry<?> findEntry(final String key);
}