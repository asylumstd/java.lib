/*
 * Copyright (c) 2011 Asylum Studios Romania.
 */

package org.asylum;

import java.util.Collection;
import java.util.ArrayList;

/**
 * Author: adrian.libotean
 * Date: 08/04/2011
 * Time: 12:41:12 PM
 * Summary:
 */
public class CollectionsUtil {

    public static <T> Collection<T> toCollection(final Iterable<T> items) {
        final Collection<T> result = new ArrayList<T>();

        for (T item : items) {
            if (item != null) {
                result.add(item);
            }
        }

        return result;
    }
}