/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum;

/**
 * Author: adrian.libotean
 * Date: May 15, 2007
 * Time: 10:59:52 AM
 * Summary: Ignores all output
 */
public class NullConsole implements Console {
    public void startUp() {}
    public void shutdown() {}
    public Console print(CharSequence sequence) { return this; }
    public Console println(CharSequence sequence) { return this; }
    public void flush() {}
    public void error(String message) {}
    public void error(String message, Throwable ex) {}
    public void warning(String message) {}
    public void info(String message) {}
}