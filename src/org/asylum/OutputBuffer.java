/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum;

/**
 * Author: adrian.libotean
 * Date: Dec 10, 2009
 * Time: 5:04:21 PM
  */
public interface OutputBuffer {
    int capacity();
    int remaining();

    void rewind();
    void position(int position);
    void mark();
    void compact();
    void flush();

    void put(String text);
    void put(String text, int index, int length);
}