/*
 * Copyright (c) 2009 Asylum Studios Romania.
 */

package org.asylum.lang;

import org.asylum.Console;
import org.asylum.SystemConsole;
import org.asylum.services.ServiceHandlerNotRegisteredError;
import org.asylum.services.ServiceRegistry;

import org.testng.*;
import org.testng.annotations.*;

/**
 * Author: adrian.libotean
 * Date: Dec 2, 2009
 * Time: 12:05:17 PM
 */
public class DisposerTest {

    @BeforeClass
    public void setup() {
        try {
            ServiceRegistry.getService(Console.class);
        } catch (ServiceHandlerNotRegisteredError ex) {
            ServiceRegistry.register(Console.class, new SystemConsole());
        }
    }

    @Test
    public void simple() {
        final Disposable parent = new Disposable() {
            public void dispose() {
                Disposer.dispose(this);
            }
        };

        final Disposable child1 = new Disposable() {
            public void dispose() {
                Disposer.dispose(this);
            }
        };

        final Disposable child2 = new Disposable() {
            public void dispose() {
                Disposer.dispose(this);
            }
        };


        Disposer.register(parent);
        Disposer.register(parent, child1);
        Disposer.register(parent, child2);

        Assert.assertTrue(Disposer.contains(parent));
        Assert.assertTrue(Disposer.contains(child1));
        Assert.assertTrue(Disposer.contains(child2));
        Assert.assertEquals(Disposer.getChildrenCount(parent), 2);

        Disposer.dispose(parent);
        
        Assert.assertFalse(Disposer.contains(parent));
        Assert.assertFalse(Disposer.contains(child1));
        Assert.assertFalse(Disposer.contains(child2));

        Disposer.register(parent, child1);
        Disposer.register(parent, child2);

        Assert.assertTrue(Disposer.contains(parent));
        Assert.assertTrue(Disposer.contains(child1));
        Assert.assertTrue(Disposer.contains(child2));
        Assert.assertEquals(Disposer.getChildrenCount(parent), 2);

        child1.dispose();
        parent.dispose();
        
        Assert.assertFalse(Disposer.contains(parent));
        Assert.assertFalse(Disposer.contains(child1));
        Assert.assertFalse(Disposer.contains(child2));
    }

    @Test
    public void leakChild() {
        final Disposable parent = new Disposable() {
            public void dispose() {
                Disposer.dispose(this);
            }
        };

        final Disposable child1 = new Disposable() {
            public void dispose() {
                Disposer.dispose(this);
            }
        };

        Disposer.register(parent, child1);

        Assert.assertTrue(Disposer.contains(parent));
        Assert.assertTrue(Disposer.contains(child1));
        Assert.assertEquals(Disposer.getChildrenCount(parent), 1);
    }

    @Test
    public void leakParent() {
        final Disposable parent = new Disposable() {
            public void dispose() {
                Disposer.dispose(this);
            }
        };

        Disposer.register(parent);

        Assert.assertTrue(Disposer.contains(parent));
        Assert.assertEquals(Disposer.getChildrenCount(parent), 0);
    }
}